/*
 * Initialize the encrypted channel
 * 
 * GPL v.3
 * 
 * Francesco De Angelis
 * 
 * francesco.deangelis@unige.ch
 * 
 * 
 */
package saperedemo;

import java.util.Vector;
import java.util.logging.Logger;
import org.apache.commons.codec.binary.Base64;
import sapere.agent.SapereAgent;
import sapere.lsa.Property;
import sapere.lsa.interfaces.ILsa;
import sapere.node.NodeManager;
import sapere.node.notifier.event.BondAddedEvent;
import sapere.node.notifier.event.BondRemovedEvent;
import sapere.node.notifier.event.BondedLsaUpdateEvent;
import sapere.node.notifier.event.DecayedEvent;
import sapere.node.notifier.event.PropagationEvent;
import sapere.node.notifier.event.ReadEvent;
import saperedemo.securitymanager.EncryptedMessage;
import saperedemo.securitymanager.KeyManager;

public class AgentInjector extends SapereAgent {

	private static final Logger LOGGER = Logger.getLogger("extension.agent.example");

	private NodeManager nManager;
	private String nodeName;
	private GAgent gradient;
	private KeyManager key;

	public AgentInjector(String agentName, String nodeName, NodeManager a_nodeManager) {
		super(agentName, a_nodeManager.getOperationManager(), a_nodeManager.getNotifier());
		this.nodeName = nodeName;
		this.nManager = a_nodeManager;

		try {
			// create a new key manager
			key = new KeyManager();
		} catch (Exception ex) {
			System.err.println("Error: " + ex.getMessage());
		}

	}

	public void onBondRemovedNotification(BondRemovedEvent event) {
	}

	public void onPropagationEvent(PropagationEvent event) {
	}

	public void onReadNotification(ReadEvent readEvent) {
	}

	public void onDecayedNotification(DecayedEvent event) {
		this.lsaId = null;
	}

	@Override
	public void setInitialLSA() {
		lsa.addProperty(new Property("Agent", this.agentName));
		// we are waiting for an encrypted message
		lsa.addSubDescription("SubProp", new Property(Const.PROP_ENC_MEX, "*"));
		createGradient();
		submitOperation();
	}

	/*
	 * Create a gradient containing the public key of this node
	 */
	private void createGradient() {
		Vector<Property> pv = new Vector<Property>();

		Base64 encoder = new Base64();
		byte[] toEncode = Base64.encodeBase64(key.getGeneratedPublicKey().getEncoded());

		// encode the public key
		String encodedPublicKey = Base64.encodeBase64String(key.getGeneratedPublicKey().getEncoded());

		Property p = new Property(Const.PROP_PUB_KEY, encodedPublicKey);

		pv.add(p);
		// create a gradient agent
		gradient = new GAgent(agentName + "Gradient", nManager.getOperationManager(), nManager.getNotifier(),
				this.nodeName, pv);
		gradient.setInitialLSA();
	}

	@Override
	public void onBondedLsaUpdateEventNotification(BondedLsaUpdateEvent event) {

		if (isLsaInteresting(event.getLsa()) == false)
			return;

	}

	@Override
	public void onBondAddedNotification(BondAddedEvent event) {
		// TODO Auto-generated method stub
		if (isLsaInteresting(event.getBondedLsa()) == false)
			return;

		// get the encrypted message
		String encMex = event.getBondedLsa().getProperty(Const.PROP_ENC_MEX).getValue().elementAt(0);
		// print the encrypted message
		EncryptedMessage mex = EncryptedMessage.decode(encMex);

		try {
			EncryptedMessage decMex = key.decMessage(mex);
			System.out.println("Node 0 chemotaxis value: " + new String(decMex.body, "UTF-8"));
		} catch (Exception ex) {
			System.out.println("Error: " + ex.getMessage());
		}

		// stop spreading the gradient across the network
		// gradient.destroy();
	}

	public boolean isLsaInteresting(ILsa lsa) {
		if (DGAgent.isExternalEvent(lsa) == false)
			return false;
		if (lsa.getProperty("EncMex") != null)
			return true;

		return false;
	}
}
