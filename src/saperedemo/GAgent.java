/*
 * The agent used to implement a dynamic gradient for the secured channel service.
 * 
 * GPL v.3
 * 
 * Francesco De Angelis
 * 
 * francesco.deangelis@unige.ch
 * 
 * 
 */

package saperedemo;

import java.util.UUID;
import java.util.Vector;
import extension.SapereProperties;
import sapere.lsa.Property;
import sapere.lsa.interfaces.ILsa;
import sapere.lsa.values.PropertyName;
import sapere.node.lsaspace.OperationManager;
import sapere.node.notifier.Notifier;
import sapere.node.notifier.event.BondAddedEvent;
import sapere.node.notifier.event.BondRemovedEvent;
import sapere.node.notifier.event.BondedLsaUpdateEvent;
import sapere.node.notifier.event.DecayedEvent;
import sapere.node.notifier.event.PropagationEvent;
import sapere.node.notifier.event.ReadEvent;

public class GAgent extends DGAgent {

	private String gradientId;
	private Vector<Property> props = null;

	public GAgent(String name, OperationManager an_opMng, Notifier notifier, String gradientId,
			Vector<Property> props) {
		super(name, an_opMng, notifier);
		this.gradientId = gradientId;
		if (props != null)
			this.props = (Vector<Property>) props.clone();
	}

	public void addDynamicGradient() {
		// this is a dynamic gradient
		lsa.addProperty(new Property("agent-name", this.agentName));
		lsa.addProperty(new Property("name", gradientId));
		lsa.addProperty(new Property("gradient", gradientId));
		lsa.addProperty(new Property(PropertyName.GRADIENT.toString(), gradientId));
		lsa.addProperty(new Property("uuid", UUID.randomUUID().toString()));
		lsa.addProperty(new Property("spread", "true"));
		lsa.addProperty(new Property(PropertyName.GRADIENT_PREVIOUS.toString(),
				PropertyName.GRADIENT_PREVIOUS_LOCAL.toString()));
		lsa.addProperty(new Property(PropertyName.GRADIENT_HOP.toString(), "0"));
		lsa.addProperty(
				new Property(PropertyName.GRADIENT_MAX_HOP.toString(), "" + SapereProperties.DEFAULT_GRADIENT_MAX_HOP));
		lsa.addSubDescription("q", new Property("data-value", "datum"));
		lsa.addProperty(new Property("decay", SapereProperties.GRADIENT_DECAY_VALUE));
	}

	public String getPersonalGradientId(ILsa lsa) {
		return gradientId;
	}

	@Override
	public void setInitialLSA() {
		// TODO Auto-generated method stub
		createLsa(LSAInit.START);
	}

	public void createLsa(LSAInit reason) {
		// add user properties to the lsa
		if (this.props != null) {
			for (Property p : this.props)
				lsa.addProperty(p);
		}
		// add all the properties for the dynamic gradient
		addDynamicGradient();
		lsa.addProperty(new Property("AGradient", this.agentName));
		submitOperation();
	}

	@Override
	public void onDecayedNotification(DecayedEvent event) {
		// System.out.println(agentName + " decayedEvent");
		super.onDecayedNotification(event);

		// change the gradient color
		// MapGraphic.c.setNewColor();
	}

	@Override
	public void onBondAddedNotification(BondAddedEvent event) {
		// TODO Auto-generated method stub
	}

	@Override
	public void onBondRemovedNotification(BondRemovedEvent event) {
		// TODO Auto-generated method stub
	}

	@Override
	public void onBondedLsaUpdateEventNotification(BondedLsaUpdateEvent event) {
		// TODO Auto-generated method stub
	}

	@Override
	public void onPropagationEvent(PropagationEvent event) {
		// TODO Auto-generated method stub
	}

	@Override
	public void onReadNotification(ReadEvent readEvent) {
		// TODO Auto-generated method stub
	}
}
