package lectures.applications;

/*
 * 
 * This class generates the SAPERE agents for the simulation
 * 
 */

import lectures.agents.AgentBondingQuestion;
import lectures.agents.AgentDecay;
import one.core.Application;
import one.core.Settings;
import eu.sapere_project.wp3lib.application.SapereApplication;
public final class GeneratorAgentBondingQuestionDecay extends SapereApplication {
	
	
	public GeneratorAgentBondingQuestionDecay(final Settings some_settings) {
		super(some_settings);
	}

	public void setupA0() 
	{	
		
		AgentDecay agentDecay = new AgentDecay("A1decay", getNodeManager());
		agentDecay.setInitialLSA();
		
		// agent that reads the decay value of agentDecay
		AgentBondingQuestion agent = new AgentBondingQuestion("A1", "decay", getNodeManager());
		agent.setInitialLSA();
	}
	public void setupAll()
	{
	}
	
	/**
	 * {@inheritDoc}
	 */
	@Override
	public Application replicate() {
		return new GeneratorAgentBondingQuestionDecay(getSettings());
	}
}
