package lectures.applications;

/*
 * 
 * This class generates the SAPERE agents for the simulation
 * 
 */

import lectures.agents.AgentAggregation;
import lectures.agents.AgentAggregationInsertValue;
import one.core.Application;
import one.core.Settings;
import eu.sapere_project.wp3lib.application.SapereApplication;
public final class GeneratorAgentAggregation extends SapereApplication {
	
	public GeneratorAgentAggregation(final Settings some_settings) {
		super(some_settings);
	}

	public void setupA0() 
	{	
		// we create some agents injecting values in the LSA space
		for (int i = 0; i < 4; i++)
		{
			new AgentAggregationInsertValue("A0aggregator"+i, getNodeManager(), "" + 2*i).setInitialLSA();
		}
		
		// this is the agent that aggregates the injected values
		AgentAggregation agent = new AgentAggregation("A0aggregatorReader", getNodeManager());
		agent.setInitialLSA();
	}
	public void setupAll()
	{
	}
	
	/**
	 * {@inheritDoc}
	 */
	@Override
	public Application replicate() {
		return new GeneratorAgentAggregation(getSettings());
	}
}
