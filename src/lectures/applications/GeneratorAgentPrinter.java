package lectures.applications;

/*
 * 
 * This class generates the SAPERE agents for the simulation
 * 
 */

import lectures.agents.AgentPrint;
import one.core.Application;
import one.core.Settings;
import eu.sapere_project.wp3lib.application.SapereApplication;

public final class GeneratorAgentPrinter extends SapereApplication {
	
	public GeneratorAgentPrinter(final Settings some_settings) {
		super(some_settings);
	}
	/**
	 * Creates an initial Lsa in node 0.
	 */
	public void setupA0() 
	{	
		AgentPrint agent = new AgentPrint("AgentName1", getNodeManager());
		agent.setInitialLSA();
	}
	
	public void setupA1()
	{
		AgentPrint agent = new AgentPrint("AgentName2", getNodeManager());
		agent.setInitialLSA();
	}
	
	public void setupB3()
	{
		AgentPrint agent = new AgentPrint("AgentName3", getNodeManager());
		agent.setInitialLSA();
	} 
	
	/**
	 * {@inheritDoc}
	 */
	@Override
	public Application replicate() {
		return new GeneratorAgentPrinter(getSettings());
	}
}
