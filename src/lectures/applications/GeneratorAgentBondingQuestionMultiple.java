package lectures.applications;

/*
 * 
 * This class generates the SAPERE agents for the simulation
 * 
 */

import lectures.agents.AgentBondingQuestion;
import lectures.agents.AgentSingleProperty;
import one.core.Application;
import one.core.Settings;
import eu.sapere_project.wp3lib.application.SapereApplication;

public final class GeneratorAgentBondingQuestionMultiple extends SapereApplication {
	
	public GeneratorAgentBondingQuestionMultiple(final Settings some_settings) {
		super(some_settings);
	}

	public void setupA0() 
	{	

		AgentBondingQuestion agent = new AgentBondingQuestion("A0", "neighbour", getNodeManager());
		agent.setInitialLSA();
		
		AgentSingleProperty agent2 = new AgentSingleProperty("A02","fhfhg","asdfadfadsfsad", getNodeManager());
		agent2.setInitialLSA();
		
		AgentSingleProperty agent3 = new AgentSingleProperty("A03","nodeName","A03", getNodeManager());
		agent3.setInitialLSA();
		
		AgentSingleProperty agent4 = new AgentSingleProperty("A04","nodeName","A04", getNodeManager());
		agent4.setInitialLSA();
	}
	public void setupAll()
	{
		AgentBondingQuestion agent = new AgentBondingQuestion("A0", "neighbour", getNodeManager());
		agent.setInitialLSA();
		
		AgentSingleProperty agent2 = new AgentSingleProperty("A02","fhfhg","asdfadfadsfsad", getNodeManager());
		agent2.setInitialLSA();
		
		AgentSingleProperty agent3 = new AgentSingleProperty("A03","nodeName","A03", getNodeManager());
		agent3.setInitialLSA();
		
		AgentSingleProperty agent4 = new AgentSingleProperty("A04","nodeName","A04", getNodeManager());
		agent4.setInitialLSA();
	}
	
	/**
	 * {@inheritDoc}
	 */
	@Override
	public Application replicate() {
		return new GeneratorAgentBondingQuestionMultiple(getSettings());
	}
}
