package lectures.applications;

/*
 * 
 * This class generates the SAPERE agents for the simulation
 * 
 */

import lectures.agents.AgentPrint;
import lectures.agents.AgentSingleProperty;
import one.core.Application;
import one.core.Settings;
import sapere.lsa.Lsa;
import eu.sapere_project.wp3lib.application.SapereApplication;

public final class GeneratorAgentSingleProperty extends SapereApplication {
	
	public GeneratorAgentSingleProperty(final Settings some_settings) {
		super(some_settings);
	}

	public Lsa my_lsa;

	private boolean injectWhenDeleted = false;

	public void setupA0() 
	{	
		AgentSingleProperty agent = new AgentSingleProperty("A0","nodeName","A0",getNodeManager());
		agent.setInitialLSA();
	}
	
	public void setupA1()
	{
		// we create two agents in the LSA space of node A1
		//		
		AgentSingleProperty agent = new AgentSingleProperty("A11","nodeName","A11", getNodeManager());
		agent.setInitialLSA();
		
		AgentPrint agent2 = new AgentPrint("A12", getNodeManager());
		agent2.setInitialLSA();
	}
	
	public void setupAll()
	{
	}
	
	/**
	 * {@inheritDoc}
	 */
	@Override
	public Application replicate() {
		return new GeneratorAgentSingleProperty(getSettings());
	}
}
