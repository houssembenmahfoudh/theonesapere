package lectures.applications;

/*
 * 
 * This class generates the SAPERE agents for the simulation
 * 
 */

import lectures.agents.AgentBondingStarSpreading;
import lectures.agents.AgentSpreading;
import one.core.Application;
import one.core.Settings;
import eu.sapere_project.wp3lib.application.SapereApplication;

public final class GeneratorAgentSpreading extends SapereApplication {

	public GeneratorAgentSpreading(final Settings some_settings) {
		super(some_settings);
	}

	public void setupA0() 
	{	
		AgentSpreading agent = new AgentSpreading("A0spreading", getNodeManager(),"here A0spreading!");
		agent.setInitialLSA();
	}
	
	public void setupAllB(String nodeName)
	{
		AgentBondingStarSpreading agent = new AgentBondingStarSpreading("receiver"+nodeName, getNodeManager());
		agent.setInitialLSA();
	}
	
	public void setupAll()
	{
	}
	
	/**
	 * {@inheritDoc}
	 */
	@Override
	public Application replicate() {
		return new GeneratorAgentSpreading(getSettings());
	}
}
