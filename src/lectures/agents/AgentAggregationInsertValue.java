package lectures.agents;

/*
 * This is an agent that injects a value to be aggregated by agents 
 * of class AgentAggregation.
 */
import sapere.agent.SapereAgent;
import sapere.lsa.Property;
import sapere.node.NodeManager;
import sapere.node.notifier.event.BondAddedEvent;
import sapere.node.notifier.event.BondRemovedEvent;
import sapere.node.notifier.event.BondedLsaUpdateEvent;
import sapere.node.notifier.event.DecayedEvent;
import sapere.node.notifier.event.PropagationEvent;
import sapere.node.notifier.event.ReadEvent;

public class AgentAggregationInsertValue extends SapereAgent {

   private final String agentName;
   private String value;

   /**
    * Creates a new agent.
    * @param name the name of the node
    * @param an_opMng the operation manager for the node.
    * @param value the information to be aggregated.
    */
   public AgentAggregationInsertValue(String name, NodeManager a_nodeManager, String value) {
      super(name, a_nodeManager.getOperationManager(), a_nodeManager.getNotifier());
      agentName = name;
      this.value=value;
   }

   /**
    * {@inheritDoc}
    */
   public void onBondAddedNotification(BondAddedEvent event)
   {
   }

   /**
    * {@inheritDoc}
    */
   public void onBondRemovedNotification(BondRemovedEvent event) 
   {
   }

   /**
    * {@inheritDoc}
    */ 
   public void onBondedLsaUpdateEventNotification(BondedLsaUpdateEvent event) 
   {
   }

   /**
    * {@inheritDoc}
    */
   public void onPropagationEvent(PropagationEvent event) {
   }

   /**
    * {@inheritDoc}
    */
   public void setInitialLSA() {
	   System.out.println("Created agent " + agentName );
	  
	   // we insert a value in the property
	   addProperty(new Property("aggregationProperty", value));
	   // NOTE: do not forget to invoke this method to 
	   // update the LSA of the agent
	   submitOperation();
   }

   public void onReadNotification(ReadEvent readEvent) {
	// TODO Auto-generated method stub
   }

   public void onDecayedNotification(DecayedEvent event) {
	// TODO Auto-generated method stub
   }
}