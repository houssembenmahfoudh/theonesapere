package lectures.agents;

/*
 * This is an agent that creates a custom property in its
 * LSA to trigger bond notification with the star operator.
 */
import sapere.agent.SapereAgent;
import sapere.lsa.Property;
import sapere.lsa.interfaces.ILsa;
import sapere.node.NodeManager;
import sapere.node.notifier.event.BondAddedEvent;
import sapere.node.notifier.event.BondRemovedEvent;
import sapere.node.notifier.event.BondedLsaUpdateEvent;
import sapere.node.notifier.event.DecayedEvent;
import sapere.node.notifier.event.PropagationEvent;
import sapere.node.notifier.event.ReadEvent;

public class AgentBondingStar extends SapereAgent {

   private final String agentName;
   private final String propertyToRead;
   

   /**
    * Creates a new agent.
    * @param name the name of the node
    * @param an_opMng the operation manager for the node.
    */
   public AgentBondingStar(String name, String propertyToRead, NodeManager a_nodeManager) {
      super(name, a_nodeManager.getOperationManager(), a_nodeManager.getNotifier());
      agentName = name;
      this.propertyToRead = propertyToRead;
   }

   /**
    * {@inheritDoc}
    */
   public void onBondAddedNotification(BondAddedEvent event)
   {
	   // get the reference to the bound LSA
	   ILsa boundLsa = event.getBondedLsa();
	   // read the property of the bound LSA
	   Property prop = boundLsa.getProperty(propertyToRead);
	   
	   // NOTE: if we put several *,? operators, we have to check
	   // what is the property that triggered the bond.
	   // In this case this is useless but we keep checking that 
	   // we have received a bond because of an LSA with a property
	   // neighbour
	   if (prop == null)
		   return;
	   // NOTE: each property contain 1 name and n>=1 values
	   // this means that the content of the property is a vector
	   // of n elements.
	   // we get the first value
	   String value = prop.getValue().firstElement();
	   
	   System.out.println("Property = " + value);
   }

   /**
    * {@inheritDoc}
    */
   public void onBondRemovedNotification(BondRemovedEvent event) 
   {
	   // get the reference to the LSA that is in charge of triggering
	   // the removal notification.
	   // It may be an existing LSA or a removed LSA.
	   // NOTE: if this LSA does not exist anymore in the LSA space
	   // then it is a temporary copy that will be deleted after
	   // this call to the method onBondRemovedNotification
	   ILsa boundLsa = event.getCopyRemovedLsa();
	   
	   // read the property of the removed LSA
	   Property prop = boundLsa.getProperty(propertyToRead);
	   
	   String value = prop.getValue().firstElement();
	   
	   System.out.println("Removed = " + value);

   }

   /**
    * {@inheritDoc}
    */ 
   public void onBondedLsaUpdateEventNotification(BondedLsaUpdateEvent event) {

   }

   /**
    * {@inheritDoc}
    */
   public void onPropagationEvent(PropagationEvent event) {
   }

   /**
    * {@inheritDoc}
    */
   public void setInitialLSA() {
	   System.out.println("Created agent " + agentName );
	   
	   // we create a property to trigger the bonding eco-law
	   // w.r.t. the default LSA that express the neighbours 
	   // in the communication area
	   // NOTE: we have to put it in a subdescription!	   
	   addSubDescription("sub", new Property(propertyToRead,"*"));
	   
	   // NOTE: do not forget to invoke this method to 
	   // update the LSA of the agent
	   submitOperation();
   }

   public void onReadNotification(ReadEvent readEvent) {
	// TODO Auto-generated method stub
   }

   public void onDecayedNotification(DecayedEvent event) {
	// TODO Auto-generated method stub
   }
}