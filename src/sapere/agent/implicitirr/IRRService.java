package sapere.agent.implicitirr;

import java.util.Vector;

import sapere.lsa.Lsa;

/**
 * Interface for Implicit Request-Response Services.
 * 
 * @author Gabriella Castelli (UNIMORE)
 *
 */
public interface IRRService {
	
	/**
	 * Called on bond. This method is used to provide the service.
	 * 
	 * @param bondedLsa
	 * @return
	 */
	public Vector<String> provideResponse (Lsa bondedLsa );

}
