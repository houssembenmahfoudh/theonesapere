package sapere.agent;

import java.util.UUID;

import extension.SapereProperties;

import sapere.lsa.Lsa;
import sapere.lsa.Property;
import sapere.lsa.autoupdate.EventGenerator;
import sapere.lsa.autoupdate.PropertyValueEvent;
import sapere.lsa.autoupdate.PropertyValueListener;
import sapere.lsa.exception.UnresolvedPropertyNameException;
import sapere.lsa.values.AggregationOperators;
import sapere.lsa.values.PropertyName;
import sapere.node.lsaspace.OperationManager;
import sapere.node.notifier.Notifier;

/**
 * The abstract class that realize an agent that manages an LSA. The Agent is represented by an
 * implicit LSA, each operation on the LSA is automatically and transparently propagated to the LSA
 * space.
 * @author Gabriella Castelli (UNIMORE)
 */
public abstract class SapereAgent extends LsaAgent implements PropertyValueListener {

   /**
    * Instantiates the Smart Sapere Agent
    * @param name The name of the Agent
    */
   public SapereAgent(String name, OperationManager an_opMng, Notifier notifier) {
      super(name, an_opMng, notifier);
   }

   /**
    * Use this method to set the initial content of the LSA managed by this SapereAgent.
    */
   public abstract void setInitialLSA();

   /**
    * Adds Properties to the LSA managed by this SapereAgent
    * @param properties The Properties to be added
    */
   public void addProperty(Property... properties) {

      Property p[] = properties;
      for (int i = 0; i < p.length; i++) {
         lsa.addProperty(p[i]);
      }

      submitOperation();

   }

   /**
    * Adds a Property to the LSA managed by this SapereAgent
    * @param p The Property to be added
    */
   public void addProperty(Property p) {
      lsa.addProperty(p);
      submitOperation();
   }   
   public void addDirectPropagation(String destinationNode) {
	      this.addProperty(new Property(PropertyName.DIFFUSION_OP.toString(), "direct"), 
	    		  new Property(PropertyName.DESTINATION.toString(), destinationNode),
	    		  new Property("uid", UUID.randomUUID().toString()));
	   }
	   
	   public void addDirectPropagationOnce(String destinationNode) {
		      this.addProperty(new Property(PropertyName.DIFFUSION_OP.toString(), "direct"), 
		    		  new Property(PropertyName.DESTINATION.toString(), destinationNode),
		    		  new Property("uid", UUID.randomUUID().toString()),
		    		  new Property(PropertyName.DIFFUSION_ONCE.toString(), "yes"));
		   }

	   public void addDirectPropagation() {
	      this.addProperty(new Property(PropertyName.DIFFUSION_OP.toString(), "direct"), 
	    		  new Property(PropertyName.DESTINATION.toString(), "all"),
	    		  new Property("uid", UUID.randomUUID().toString()));
	   }
	   
	   public void addDirectPropagationOnce() {
		      this.addProperty(new Property(PropertyName.DIFFUSION_OP.toString(), "direct"), 
		    		  new Property(PropertyName.DESTINATION.toString(), "all"),
		    		  new Property("uid", UUID.randomUUID().toString()),
		    		  new Property(PropertyName.DIFFUSION_ONCE.toString(), "yes"));
		   }
		   

   public void addDynamicGradient(String gradientId)
   {
		// this is a dynamic gradient
		lsa.addProperty(new Property("agent-name", this.agentName));
		lsa.addProperty(new Property("name", gradientId));
		//lsa.addProperty(new Property("gradient", gradientId));
		lsa.addProperty(new Property(PropertyName.GRADIENT.toString(), gradientId));
		//lsa.addProperty(new Property("uuid", UUID.randomUUID().toString()));
		//lsa.addProperty(new Property("spread", "true"));
		lsa.addProperty(new Property(PropertyName.GRADIENT_PREVIOUS.toString(), 
									PropertyName.GRADIENT_PREVIOUS_LOCAL.toString()));
		lsa.addProperty(new Property(PropertyName.GRADIENT_HOP.toString(), "0"));
		// default gradient max hop
		lsa.addProperty(new Property(PropertyName.GRADIENT_MAX_HOP.toString(),""+		
				SapereProperties.DEFAULT_GRADIENT_MAX_HOP));
		lsa.addSubDescription("q", new Property("data-value", "datum"));
		lsa.addProperty(new Property("decay", SapereProperties.GRADIENT_DECAY_VALUE));
   }
   
   public void addDynamicGradient(String gradientId, int maxHop)
   {
		// this is a dynamic gradient
		lsa.addProperty(new Property("agent-name", this.agentName));
		lsa.addProperty(new Property("name", gradientId));
		//lsa.addProperty(new Property("gradient", gradientId));
		lsa.addProperty(new Property(PropertyName.DIFFUSION_OP.toString(), "GRADIENT"));
		lsa.addProperty(new Property(PropertyName.GRADIENT.toString(), gradientId));
		//lsa.addProperty(new Property("uuid", UUID.randomUUID().toString()));
		//lsa.addProperty(new Property("spread", "true"));
		
		lsa.addProperty(new Property(PropertyName.GRADIENT_PREVIOUS.toString(), 
				PropertyName.GRADIENT_PREVIOUS_LOCAL.toString()));
		lsa.addProperty(new Property(PropertyName.GRADIENT_HOP.toString(), "0"));
		lsa.addProperty(new Property(PropertyName.SOURCE.toString(), agentName));
		// default gradient max hop
		lsa.addProperty(new Property(PropertyName.GRADIENT_MAX_HOP.toString(),""+		
				maxHop));
		lsa.addSubDescription("q", new Property("data-value", "datum"));
		lsa.addProperty(new Property("decay", ""+maxHop));
   }
	   
   public void addGradient(int nHop, String aggregationOperator, String gradientFieldName) {
      addProperty(new Property(PropertyName.DIFFUSION_OP.toString(), "GRADIENT_" + nHop), 
    		  new Property(PropertyName.AGGREGATION_OP.toString(), aggregationOperator),
    			new Property(PropertyName.GRADIENT_HOP.toString(), "0"),
    			new Property(PropertyName.GRADIENT.toString(), "id"),
    			new Property(PropertyName.GRADIENT_MAX_HOP.toString(),""+		
    					3000),
    			new Property(PropertyName.GRADIENT_PREVIOUS.toString(), 
						PropertyName.GRADIENT_PREVIOUS_LOCAL.toString()),
    			new Property(PropertyName.FIELD_VALUE.toString(), gradientFieldName),
            new Property(PropertyName.SOURCE.toString(), this.agentName + "@" + "getNodeManager().getSpaceName()"),
            new Property(PropertyName.PREVIOUS.toString(), "local"), new Property(PropertyName.DESTINATION.toString(),
                  "default"), new Property(gradientFieldName, "" + nHop));
   }

   public void addOtherAggregation(String fieldName, String AggregationOperator) {
      addProperty(new Property(PropertyName.AGGREGATION_OP.toString(), AggregationOperator));
      addProperty(new Property(PropertyName.FIELD_VALUE.toString(), fieldName));
   }

   public void addSelfAggregation(String fieldName, String AggregationOperator, String aggregationSource) {
      addProperty(new Property(PropertyName.AGGREGATION_OP.toString(), AggregationOperator));
      addProperty(new Property(PropertyName.FIELD_VALUE.toString(), fieldName));
      addProperty(new Property(PropertyName.SOURCE.toString(), aggregationSource));
   }

   public void addSelfAggregationNEWEST(String aggregationSource) {
      addProperty(new Property(PropertyName.AGGREGATION_OP.toString(), AggregationOperators.MAX.toString()));
      addProperty(new Property(PropertyName.FIELD_VALUE.toString(), "#lastModified"));
      addProperty(new Property(PropertyName.SOURCE.toString(), aggregationSource));
   }

   public void addSelfAggregationOLDEST(String aggregationSource) {
      addProperty(new Property(PropertyName.AGGREGATION_OP.toString(), AggregationOperators.MIN.toString()));
      addProperty(new Property(PropertyName.FIELD_VALUE.toString(), "#lastModified"));
      addProperty(new Property(PropertyName.SOURCE.toString(), aggregationSource));
   }

   public void addDecay(int decayValue) {
      addProperty(new Property(PropertyName.DECAY.toString(), "" + decayValue));
   }

   /**
    * Adds a Property that automatically updates
    * @param name The name of the Property
    * @param eg The generator of automatically updates
    */
   public void addProperty(String name, EventGenerator eg) {
      eg.setPropertyName(name);
      eg.addPropertyValueListener(this);
   }

   /**
    * Sets the value of an existing Property
    * @param p The new Property value
    */
   public void setProperty(Property p) {

      try {
         lsa.setProperty(p);
         submitOperation();
      } catch (UnresolvedPropertyNameException e) {
         // TODO Auto-generated catch block
         e.printStackTrace();
      }

   }

   /**
    * Removes a Property from the managed LSA
    * @param name
    */
   public void removeProperty(String name) {
      lsa.removeProperty(name);
      submitOperation();
   }

   /**
    * Adds a SubDescription with a single Property
    * @param name
    * @param value
    */
   public void addSubDescription(String name, Property value) {
      lsa.addSubDescription(name, value);
      submitOperation();
   }

   /**
    * Adds a SubDescription with a variable number of Properties
    * @param name
    * @param values
    */
   public void addSubDescription(String name, Property... values) {
      // SubDescription s = null;
      lsa.addSubDescription(name, values);

      // s.toString();

      submitOperation();
   }

   /**
    * Removes the specified SubDescription from the managed LSA
    * @param name
    */
   public void removeSubDescription(String name) {
      lsa.removeSubDescription(name);
      submitOperation();
   }

   /**
    * Stes the value of an existing SubDescription
    * @param name
    * @param value
    */
   public void setSubDescription(String name, Property value) {
      lsa.setSubDescription(name, value);
      submitOperation();
   }

   public void PropertyValueGenerated(PropertyValueEvent event) {
      if (!((Lsa) lsa).hasProperty(event.getPropertyName())) {
         addProperty(new Property(event.getPropertyName(), event.getValue()));
      } else {
         // update
         setProperty(new Property(event.getPropertyName(), event.getValue()));
      }
   }

   public void PropertyValueAppendGenerated(PropertyValueEvent event) {
      if (!((Lsa) lsa).hasProperty(event.getPropertyName())) {
         addProperty(new Property(event.getPropertyName(), event.getValue()));
      } else {
         // update
         // setProperty(new Property(event.getPropertyName(), event.getValue()));
         Property p = lsa.getProperty(event.getPropertyName());
         p.getPropertyValue().addValue(event.getValue());
         setProperty(new Property(event.getPropertyName(), p.getValue()));

      }

   }

}
