package sapere.agent;

import sapere.node.notifier.event.BondAddedEvent;
import sapere.node.notifier.event.BondRemovedEvent;
import sapere.node.notifier.event.BondedLsaUpdateEvent;
import sapere.node.notifier.event.DecayedEvent;
import sapere.node.notifier.event.PropagationEvent;
import sapere.node.notifier.event.ReadEvent;

/**
 * Interface with methods triggered on Events
 * 
 * @author Gabriella Castelli (UNIMORE)
 *
 */
public interface ISapereAgent {
	
	/**
	 * Triggered when a new Bond happens
	 * 
	 * @param event The BondAddedEvent
	 */
	public void onBondAddedNotification(BondAddedEvent event);

	/**
	 * Triggered when a de bond happens
	 * 
	 * @param event The BondRemovedEvent
	 */
	public void onBondRemovedNotification(BondRemovedEvent event);

	/**
	 * Triggered when a binded LSA has been updated 
	 * 
	 * @param event The BondedLsaUpdateEvent
	 */
	public void onBondedLsaUpdateEventNotification(BondedLsaUpdateEvent event);
	
	/**
	 * Triggered when a LSA is removed by the Decay Eco-law
	 * 
	 * @param event The DecayedEvent
	 */
	public void onDecayedNotification(DecayedEvent event);
	
	/**
	 * Triggerd when a LSA is propagated by the Propagation Eco-law
	 * 
	 * @param event The PropagationEvent
	 */
	public void onPropagationEvent (PropagationEvent event);
	
	/**
	 * Called when a Read Operation is performed
	 * 
	 * @param readEvent The readEvent that contains the LSA read
	 */
	public void onReadNotification(ReadEvent readEvent);
	
	//public void onUpdateNotification(UpdateEvent event);

	//public void onLsaExpiredNotification(LsaExpiredEvent event);
	
	//public void onAggregationRemovedNotification(AggregationRemovedEvent event);



}
