package sapere.agent;

import sapere.lsa.Id;
import sapere.lsa.Lsa;
import sapere.lsa.interfaces.ILsa;
import sapere.node.lsaspace.Agent;
import sapere.node.lsaspace.Operation;
import sapere.node.lsaspace.OperationManager;
import sapere.node.notifier.INotifier;
import sapere.node.notifier.Notifier;
import sapere.node.notifier.Subscription;
import sapere.node.notifier.event.BondAddedEvent;
import sapere.node.notifier.event.BondRemovedEvent;
import sapere.node.notifier.event.BondedLsaUpdateEvent;
import sapere.node.notifier.event.DecayedEvent;
import sapere.node.notifier.event.IEvent;
import sapere.node.notifier.event.PropagationEvent;
import sapere.node.notifier.event.ReadEvent;
import sapere.node.notifier.filter.BondedLsaUpdateFilter;
import sapere.node.notifier.filter.DecayedFilter;

/**
 * Internal class that implements actions for Agents that manages LSA
 * @author Gabriella Castelli (UNIMORE)
 */
public abstract class LsaAgent extends Agent implements ISapereAgent {

   /** The managed LSA */
   protected ILsa lsa = null;

   /** The id of the maanged LSA */
   protected Id lsaId = null;

   /** The OperationManager of the local SAPERE node */
   protected OperationManager opMng = null;

   /** The Notifier of the local SAPERE node */
   protected INotifier notifier = null;

   /**
    * Instantiates this LsaAgent
    * @param agentName The name of this Agent
    */
   public LsaAgent(String agentName, OperationManager an_opMng, Notifier notifier) {
      super(agentName);

      this.lsa = new Lsa();
      this.opMng = an_opMng;
      this.notifier = notifier;
   }
   
   public void resetId()
   {
	   this.lsaId = null;
   }

   /*
    * private void submitOperation(Operation op){ if(op.getOpType() == OperationType.Inject)
    * injectOperation(op); if(op.getOpType() == OperationType.Update) updateOperation(op); }
    */

   /**
    * Removes the managed LSA from the local LSA space
    */
   public void removeLsa() {
      this.lsa = null;
      Operation op = new Operation().removeOperation(lsaId, getAgentName());
      opMng.queueOperation(op);
   }

   protected void submitOperation() {
      if (lsaId == null)
         lsaId = injectOperation();
      else
         updateOperation();
   }

   protected Id injectOperation() {
      Id lsaId = null;

      Operation op = new Operation().injectOperation((Lsa) this.lsa, getAgentName(), this);

      lsaId = opMng.queueOperation(op);
      return lsaId;
   }

   /*
    * private Id injectOperation(Operation op){ lsaId = opMng.queueOperation(op); return lsaId; }
    */

   protected void updateOperation() {
      Operation op = new Operation().updateOperation((Lsa) this.lsa, lsaId, getAgentName(), this);

      opMng.queueOperation(op);
   }
   
   protected void readOperation(Id lsaId) {
	   
	   Operation op = new Operation().readOperation(lsaId, getAgentName(), this);
	   
	   opMng.queueOperation(op);
   }

   /*
    * private void updateOperation(Operation op){ opMng.queueOperation(op); }
    */

   @Override
   public void onNotification(IEvent event) {

      
        if (event.getClass().isAssignableFrom(ReadEvent.class)){
        	ReadEvent readEvent = (ReadEvent) event;
        	onReadNotification(readEvent); }
        
        /* if
       * (event.getClass().isAssignableFrom(UpdateEvent.class)){ UpdateEvent updateEvent =
       * (UpdateEvent) event; onUpdateNotification(updateEvent); } if
       * (event.getClass().isAssignableFrom(LsaExpiredEvent.class)){ LsaExpiredEvent lsaExpiredEvent
       * = (LsaExpiredEvent) event; onLsaExpiredNotification(lsaExpiredEvent); }
       */
      if (event.getClass().isAssignableFrom(BondAddedEvent.class)) {
         BondAddedEvent bondAddedEvent = (BondAddedEvent) event;

         Id bonded = new Id(bondAddedEvent.getBondId());
         if (!Id.isSdId(bonded))
            bonded = new Id(bondAddedEvent.getBondId().substring(0, bondAddedEvent.getBondId().lastIndexOf("#")));

         BondedLsaUpdateEvent bondedLsaUpdateEvent = new BondedLsaUpdateEvent(null);
         BondedLsaUpdateFilter filter = new BondedLsaUpdateFilter(bonded, lsaId.toString());
         Subscription s = new Subscription(bondedLsaUpdateEvent.getClass(), filter, this, this.getAgentName());

         notifier.subscribe(s); 

         onBondAddedNotification(bondAddedEvent);
      }
      if (event.getClass().isAssignableFrom(BondedLsaUpdateEvent.class)) {
         BondedLsaUpdateEvent bondedLsaUpdateEvent = (BondedLsaUpdateEvent) event;
         onBondedLsaUpdateEventNotification(bondedLsaUpdateEvent);
      }
      if (event.getClass().isAssignableFrom(BondRemovedEvent.class)) {
    	  //Trigger the event
    	  BondRemovedEvent bondRemovedEvent = (BondRemovedEvent) event;
    	  onBondRemovedNotification(bondRemovedEvent);

    	  // Remove Subscription to BondedLSaUpdateEvent
    	  BondedLsaUpdateFilter filter = new BondedLsaUpdateFilter(new Id(bondRemovedEvent.getBondId()),
               lsaId.toString());
    	  notifier.unsubscribe(filter);
      }
      
      if (event.getClass().isAssignableFrom(DecayedEvent.class)){
    	  //Trigger the Event
    	  DecayedEvent decayEvent = (DecayedEvent) event;
    	  onDecayedNotification(decayEvent);

    	  //Remove the Subscription to DecayedEvent
    	  DecayedFilter filter = new DecayedFilter (lsaId, this.agentName);
    	  Subscription s = new Subscription(decayEvent.getClass(), filter, this,
        			lsaId.toString());
    	  notifier.unsubscribe(s);
        	
        } 
       /*if
        (event.getClass().isAssignableFrom(AggregationRemovedEvent.class)){ AggregationRemovedEvent
        aggregationRemovedEvent = (AggregationRemovedEvent) event;
        onAggregationRemovedNotification(aggregationRemovedEvent); }*/
       

      if (event.getClass().isAssignableFrom(PropagationEvent.class)) {
         PropagationEvent propagationEvent = (PropagationEvent) event;
         onPropagationEvent(propagationEvent);
      }

   }



@Override
   public String toString() {
      return lsa.toString();
   }

}
