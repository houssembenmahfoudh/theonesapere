
package sapere.node.lsaspace;

import java.io.Serializable;

/**
 * Enumeration for the possible type of Operation over a space
 * 
 * @author Matteo Desanti (UNIBO)
 *
 */

public enum OperationType implements Serializable{
	Inject,
	Observe,
	Remove,
	Update,
	BondUpdate,
	UpdateParametrized,
	BondUpdateParametrized,
	InjectObserve, 
	Read
}
