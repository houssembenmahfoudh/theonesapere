package sapere.node.lsaspace;

import java.util.ArrayList;
import sapere.lsa.Lsa;
import sapere.node.networking.INetworkDeliveryManager;
import sapere.node.networking.INetworkReceiverManager;

public class SyncBuffer {
	private ArrayList<Lsa> bufferLsa = new ArrayList<Lsa>();

	private INetworkReceiverManager nm;
	private INetworkDeliveryManager nm_sender;

	public void setNetworkReceiverInterface(INetworkReceiverManager nm, INetworkDeliveryManager nm_sender) {
		this.nm = nm;
		this.nm_sender = nm_sender;
	}

	public synchronized void put(Lsa lsa) {
		bufferLsa.add(lsa);
		// nm.doInject(lsa);
	}

	public synchronized void syncWithExternal() {
		for (Lsa lsa : bufferLsa) {
			// //if (nm_sender.discardLsa(lsa, 0) == false)
			// if (lsa.getProperty(PropertyName.GRADIENT.toString()) != null)
			// {
			// System.out.println("In queue: " + bufferLsa.size());
			// }
			nm.doInject(lsa);
		}
		bufferLsa.clear();
	}
}
