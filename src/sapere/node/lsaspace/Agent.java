package sapere.node.lsaspace;

import sapere.node.notifier.AbstractSubscriber;
import sapere.node.notifier.event.IEvent;

/**
 * Abstract class that represents a Sapere Agent
 * 
 * @author Gabriella Castelli (UNIMORE)
 * 
 */
public abstract class Agent extends AbstractSubscriber {

	protected String agentName = null;
	
	/**
	 * @param agentName The name of this Agent
	 */
	public Agent(String agentName){
		this.agentName = agentName;
	}

	/**
	 * Retrieves the name of the Agent
	 * 
	 * @return The name of the Agent
	 */
	public String getAgentName() {
		return agentName;
	}

	//@Override
	public void onNotification(IEvent event) {
		// TODO Auto-generated method stub
	}

}
