// [Project Title]
// (c) Copyright 2011, 2012 Graeme Stevenson (graeme.stevenson@st-andrews.ac.uk)
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions
// are met:
// 1. Redistributions of source code must retain the above copyright
// notice, this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright
// notice, this list of conditions and the following disclaimer in the
// documentation and/or other materials provided with the distribution.
// 3. The name of the author may not be used to endorse or promote products
// derived from this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
// IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
// OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
// IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
// INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
// NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
// DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
// THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
// THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
package sapere.node.lsaspace.ecolaws;

import java.util.Arrays;
import java.util.List;

import sapere.lsa.Lsa;
import sapere.node.NodeManager;
import sapere.node.lsaspace.Operation;
import sapere.node.lsaspace.Space;
import sapere.node.networking.INetworkDeliveryManager;
import sapere.node.notifier.event.AbstractSapereEvent;

/**
 * @author Graeme Stevenson (graeme.stevenson@st-andrews.ac.uk)
 * @author Gabriella Castelli (UNIMORE)
 */
public abstract class AbstractEcoLaw implements IEcoLaw {

   /**
    * The space in which the eco-law executes.
    */
   private final Space my_space;

   /**
    * The node manager for the space.
    */
   private final NodeManager my_nodeManager;

   /**
    * Constructs the Eco-Law.
    * @param a_sapce The space in which the eco-law executes.
    * @param a_networkDeliveryManager the network delivery manager for the space.
    * @param a_nodeManager the node manager for the space.
    */
   public AbstractEcoLaw(final NodeManager a_nodeManager) {
      my_space = a_nodeManager.getSpace();
      my_nodeManager = a_nodeManager;
   }

   /**
    * Returns the space name.
    *@return the space name.
    */
   protected String getSpaceName(){
      return my_space.getName();
   }
   
   /**
    * The space in which the eco-law executes.
    * @return the space
    */
   protected List<Lsa> getLSAs() {
      return Arrays.asList(my_space.getAllLsa());
   }

   /**
    * The network delivery manager of the space.
    * @return the network delivery manager
    */
   protected INetworkDeliveryManager getNetworkDeliveryManager() {
      return my_nodeManager.getNetworkDeliveryManager();
   }
   
   protected NodeManager getNodeManager() {
	      return my_nodeManager;
	   }

   /**
    * Removes an Lsa from a space.
    * @param an_lsa the lsa to remove
    * @param a_ecolawName the name of the removing ecolaw.
    */
   protected void remove(final Lsa an_lsa) {
      my_space.remove(an_lsa.getId(), getName());
   }

   /**
    * Updates an Lsa in a space.
    * @param an_lsa the lsa to update
    * @param a_ecolawName the name of the updating ecolaw.
    */
   protected void inject(final Lsa an_lsa) {
      final Operation operation = new Operation().injectOperation(an_lsa, getName(), null);
      my_nodeManager.getOperationManager().execOperation(operation);
   }

   /**
    * Updates an Lsa in a space.
    * @param an_lsa the lsa to update
    * @param a_ecolawName the name of the updating ecolaw.
    */
   protected void update(final Lsa an_lsa) {
      final Operation operation = new Operation().updateOperation(an_lsa, an_lsa.getId(), getName(), null);
      my_nodeManager.getOperationManager().execOperation(operation);
   }

   /**
    * Updates Lsa bonds in a space.
    * @param an_lsa the lsa to update
    * @param a_ecolawName the name of the updating ecolaw.
    */
   protected void updateBond(final Lsa an_lsa) {
      final Operation operation = new Operation().updateBondOperation(an_lsa, an_lsa.getId(), getName());
      my_nodeManager.getOperationManager().execOperation(operation);
   }

   /**
    * Updates Lsa parameterised bonds in a space.
    * @param an_lsa the lsa to update
    * @param a_ecolawName the name of the updating ecolaw.
    */
   protected void updateBondParamaterised(final Lsa an_lsa) {
      final Operation operation = new Operation().updateBondParametrizedOperation(an_lsa, an_lsa.getId(), getName());
      my_nodeManager.getOperationManager().execOperation(operation);
   }
   
   /**
    * Publishes an event using the notifier
    * @param an_event the event to publish
    */
   protected void publish(final AbstractSapereEvent an_event) {
      this.my_nodeManager.getNotifier().publish(an_event);
   }

}
