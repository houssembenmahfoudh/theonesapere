package sapere.node.lsaspace.ecolaws;

import extension.SapereProperties;
import sapere.lsa.Lsa;
import sapere.lsa.Property;
import sapere.lsa.exception.UnresolvedPropertyNameException;
import sapere.node.NodeManager;
import sapere.node.lsaspace.EcoLawsEngine;

/**
 * An implementation of an aggregation eco-law.
 * 
 * @author Gabriella Castelli (UNIMORE)
 */
public class Chemotaxis3 extends AbstractEcoLaw {

	public Chemotaxis3(NodeManager a_nodeManager) {
		super(a_nodeManager);
	}

	/**
	 * Iterate through the LSAs checking for gradient and direct propagation.
	 */
	public void invoke() {
		for (final Lsa lsa : getLSAs()) {
			if (isChemotaxis(lsa))
				doChemotaxisPropagation(lsa);

		}
	}

	/**
	 * Propagate an LSA using gradient propagation
	 * 
	 * @param an_lsa
	 *            the LSA to propagate.
	 */
	public void doChemotaxisPropagation(final Lsa an_lsa) {

		// Debug
		// if(! this.getSpaceName().equals("nodes0"))
		String lsaId = an_lsa.getId().toString();

		// The identifier we wish to follow.
		String chemotaxisId = an_lsa.getSingleValue("chemotaxis").get();

		// Now find a gradient LSA with matching name.
		for (final Lsa lsa : getLSAs()) {

			// if we have found a suitable matching gradient
			if (lsa.hasProperty("name") && lsa.hasProperty("gradient")
					&& chemotaxisId.equals(lsa.getSingleValue("name").get())) {

				//System.out.println(lsa);
				String previousNode = lsa.getSingleValue("previous").get();
				
				if (previousNode != null && !previousNode.equals("local")) {
					updateNHop(an_lsa);

					boolean successful = getNetworkDeliveryManager()
							.sendDirect(previousNode, an_lsa);
					if (successful) {
//						System.out.println("\t" + getSpaceName()
//								+ " is propagating a chemotaxis LSA (id:"
//								+ lsaId + ") to " + previousNode + " - "
//								+ lsa.getSingleValue("name").get());
						
						SapereProperties.hops++;
						
						if(previousNode.equals("Node0")){
							SapereProperties.messageDelivered++;
						}

						remove(an_lsa);
					}
				}
				break;
			}
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String getName() {
		return EcoLawsEngine.ECO_LAWS_PROPAGATION;
	}

	/**
	 * Whether the input LSA has chemotxis propagation properties set.
	 * 
	 * @param an_lsa
	 *            the LSA to check for gradient propagation.
	 * @return true if the LSA conforms, false otherwise.
	 */
	private boolean isChemotaxis(Lsa lsa) {
		return lsa.hasProperty("chemotaxis");
	}

	private void updateNHop(Lsa lsa) {
		if (!lsa.hasProperty("nHop")) {
			lsa.addProperty(new Property("nHop", "" + 1));
		} else {
			int currentNHop = new Integer(lsa.getProperty("nHop").getValue()
					.elementAt(0)).intValue();
			try {
				lsa.setProperty(new Property("nHop", "" + (currentNHop + 1)));
			} catch (UnresolvedPropertyNameException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}
	}

	@Override
	public boolean doesSpread() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean doesDirectPropagation() {
		// TODO Auto-generated method stub
		return true;
	}
}
