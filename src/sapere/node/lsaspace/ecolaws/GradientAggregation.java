package sapere.node.lsaspace.ecolaws;

import java.util.HashMap;
import java.util.Vector;
import sapere.lsa.Lsa;
import sapere.lsa.Property;
import sapere.lsa.SyntheticProperty;
import sapere.lsa.values.PropertyName;
import sapere.lsa.values.SyntheticPropertyName;
import sapere.node.NodeManager;
import sapere.node.lsaspace.EcoLawsEngine;

/**
 * @author Francesco De Angelis (University of Geneva)
 */
public class GradientAggregation extends AbstractEcoLaw {

	/*
	 * LSA that wants to be propagated MUST have the following fields defined in
	 * PropertyValue: aggregation_op: Aggregation Operators field_value: Field
	 * to which the Aggregation Operator will be applied source: an id to
	 * identify the LSAs to be aggregated together
	 */

	/**
	 * Creates a new instance of the aggregation eco-law.
	 * 
	 * @param a_nodeManager
	 *            the node manager of the space.
	 */
	public GradientAggregation(final NodeManager a_nodeManager) {
		super(a_nodeManager);
	}

	public void invoke() {
		// added by Francesco De Angelis
		gradientAggregation();
	}

	private HashMap<String, Vector<Lsa>> findGradientsLsas() {
		HashMap<String, Vector<Lsa>> lsas = new HashMap<String, Vector<Lsa>>();

		for (Lsa lsa : getLSAs()) {
			Property gp = lsa.getProperty(PropertyName.GRADIENT.toString());

			if (gp == null)
				continue;

			String gradientId = gp.getValue().firstElement();

			if (lsas.get(gradientId) == null)
				lsas.put(gradientId, new Vector<Lsa>());

			lsas.get(gradientId).add(lsa);
		}

		return lsas;
	}

	private void trackMinimum(Vector<Lsa> lsas, Vector<Lsa> toRemove) {
		int minValue = Integer.MAX_VALUE;
		Lsa minLsa = null;

		boolean findPrevious = false;
		for (int i = 0; i < lsas.size(); i++) {
			if (lsas.get(i).getProperty(PropertyName.GRADIENT_PREVIOUS.toString()).getValue().firstElement()
					.equals(PropertyName.GRADIENT_PREVIOUS_LOCAL)) {
				findPrevious = true;
				minLsa = lsas.get(i);
			} else {
				String strV = lsas.get(i).getProperty(PropertyName.GRADIENT_HOP.toString()).getValue().firstElement();
				int intValue = Integer.parseInt(strV);

				if (intValue < minValue) {
					minValue = intValue;
					minLsa = lsas.get(i);
				}
			}
		}

		for (int i = 0; i < lsas.size(); i++) {
			if (lsas.get(i) != minLsa)
				toRemove.add(lsas.get(i));
		}
		return;
	}

	private void gradientAggregation() {
		HashMap<String, Vector<Lsa>> gradientLsas = findGradientsLsas();

		Vector<Lsa> toRemove = new Vector<Lsa>();

		if (gradientLsas.size() == 0)
			return;

		for (String gradientId : gradientLsas.keySet()) {
			if (gradientLsas.get(gradientId).size() < 2)
				continue;

			trackMinimum(gradientLsas.get(gradientId), toRemove);
		}

		for (int i = 0; i < toRemove.size(); i++) {
			// add the creator syntethic property
			Lsa lsa = toRemove.elementAt(i);
			lsa.addProperty(new SyntheticProperty(SyntheticPropertyName.CREATOR_ID,
					EcoLawsEngine.ECO_LAWS_GRADIENT_AGGREGATION.toString()));
			remove(toRemove.elementAt(i));
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean doesSpread() {
		return false;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean doesDirectPropagation() {
		return false;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String getName() {
		return EcoLawsEngine.ECO_LAWS_GRADIENT_AGGREGATION;
	}

}
