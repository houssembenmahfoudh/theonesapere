/**
 * 
 */
package sapere.node.lsaspace.ecolaws;

import java.util.List;
import java.util.Map;

import sapere.lsa.Lsa;
import sapere.lsa.SubDescription;
import sapere.node.NodeManager;
import sapere.node.lsaspace.EcoLawsEngine;

import com.google.common.collect.Maps;

/**
 * @author Gabriella Castelli (UNIMORE)
 */
public class Bonding extends AbstractEcoLaw {
   /**
    * A cache of modified LSA clones to allow multiple updates (i.e., multiple bonds) applied to one
    * LSA on the same execution cycle. This is a workaround until the eco-law implementation is re
    * designed.
    */
   private static final Map<String, Lsa> CACHE = Maps.newHashMap();

   /**
    * Creates a new instance of the bonding eco-law.
    * @param a_nodeManager the node manager of the space.
    */
   public Bonding(final NodeManager a_nodeManager) {
      super(a_nodeManager);
   }

   /**
    * {@inheritDoc}
    */
   @Override
   public void invoke() {
      CACHE.clear();

      // Disabled - needs to be updated to work with cache. Use bonds from SD only for now.
      // execBondsFromLSA();

      execBondsFromSDToLSA();

   }

   /**
    * Executes all bonds from sub-descriptions to LSAs.
    * @param the_lsas all the LSAs
    * @param a_space the space
    * @param an_operationManager the operation manager.
    */
   private void execBondsFromSDToLSA() {
      // Iterate through all the LSAs
      for (Lsa outerLsa : getLSAs()) {
         SubDescription[] candidateSd = outerLsa.getSubDescriptions();
         // For all candidates
         for (SubDescription candidateRequester : candidateSd) {
            // If the sub description expresses a bond request
            if (isBondRequester(candidateRequester)) {
               // Flag for whether we should continue to process subdescriptions
               boolean processSubDescriptions = true;

               // If we wish only a single bond and do not currently have one established.
               if (candidateRequester.isFormalOne()) {
                  for (Lsa targetLsa : getLSAs()) {
                     if (outerLsa != targetLsa && isBondable(targetLsa) && candidateRequester.matches(targetLsa)) {
                        constructBiDirectionalBondSDToLSA(outerLsa, candidateRequester, targetLsa);
                        processSubDescriptions = false; // match found, no need to continue
                                                        // looking.
                        break;
                     }
                  }
               } else {
                  // We express a * request
                  for (Lsa targetLsa : getLSAs()) {
                     if (outerLsa != targetLsa && isBondable(targetLsa) && candidateRequester.matches(targetLsa)) {
                        constructUniDirectionalBondSDToLSA(outerLsa, candidateRequester, targetLsa);
                     }
                  }
               }
               if (processSubDescriptions) {
                  execBondsFromSDToSD(outerLsa, candidateRequester);
               }
            }
         }
      }
   }

   /**
    * Executes all bonds from sub-descriptions of an LSA to other LSAs subdescriptions.
    * @param a_subDescription the subdescription we are processing.
    * @param the_lsas all the LSAs
    * @param a_space the space
    * @param an_operationManager the operation manager.
    */
   private void execBondsFromSDToSD(final Lsa an_lsa, SubDescription a_request) {

      // If we wish only a single bond and do not currently have one established.
      if (a_request.isFormalOne()) {
         for (Lsa targetLsa : getLSAs()) {
            for (SubDescription innerSubDesc : targetLsa.getSubDescriptions()) {
               if (an_lsa != targetLsa && isBondable(innerSubDesc) && a_request.matches(innerSubDesc)) {
                  constructBiDirectionalBondSDToSD(an_lsa, a_request, targetLsa, innerSubDesc);
                  break;
               }
            }
         }
      } else {
         // We express a * request
         for (Lsa targetLsa : getLSAs()) {
            for (SubDescription innerSubDesc : targetLsa.getSubDescriptions()) {
               if (an_lsa != targetLsa && isBondable(innerSubDesc) && a_request.matches(innerSubDesc)) {
                  constructUniDirectionalBondSDToSD(an_lsa, a_request, targetLsa, innerSubDesc);
               }
            }
         }

      }
   }

   /**
    * Constructs a bond between two lsas, via a sub-description on the first.
    * @param a_bondRequestLsa the LSA containing the bond request subdescription
    * @param a_bondSubDescription the subdescription issuing the request
    * @param a_matchedLsa the lsa to bond to
    * @param an_operationManager the operation manager to perform the operation.
    */
   public void constructBiDirectionalBondSDToLSA(Lsa a_bondRequestLsa, SubDescription a_bondSubDescription,
         Lsa a_matchedLsa) {
      // Use cached versions
      final Lsa cachedRequest = getCached(a_bondRequestLsa);
      final Lsa cachedTarget = getCached(a_matchedLsa);
      final SubDescription cachedSubDescription = cachedRequest.getSubDescriptionByName(cachedRequest
            .getSubDescriptionName(a_bondSubDescription.getId()));

      // Add bonds
      cachedSubDescription.addBond(a_matchedLsa.getId().toString());
      cachedTarget.addBond(a_bondSubDescription.getId().toString());

      // Issue updates
      updateBond(cachedRequest);
      updateBond(cachedTarget);
   }

   /**
    * Constructs a bond between two lsas SDs.
    * @param a_bondRequestLsa the LSA containing the bond request subdescription
    * @param a_bondSubDescription the subdescription issuing the request
    * @param a_matchedLsa the lsa to bond to
    * @param a_matchedSubDescription the subdescription that was matched with
    * @param an_operationManager the operation manager to perform the operation.
    */
   public void constructBiDirectionalBondSDToSD(Lsa a_bondRequestLsa, SubDescription a_bondSubDescription,
         Lsa a_matchedLsa, SubDescription a_matchedSubDescription) {
      // Use cached versions
      final Lsa cachedRequest = getCached(a_bondRequestLsa);
      final Lsa cachedTarget = getCached(a_matchedLsa);
      final SubDescription cachedSubDescription = cachedRequest.getSubDescriptionByName(cachedRequest
            .getSubDescriptionName(a_bondSubDescription.getId()));
      final SubDescription cachedTargetSubDescription = cachedTarget.getSubDescriptionByName(cachedTarget
            .getSubDescriptionName(a_matchedSubDescription.getId()));

      // Add bond
      cachedSubDescription.addBond(cachedTargetSubDescription.getId().toString());
      cachedTargetSubDescription.addBond(cachedSubDescription.getId().toString());

      // Update operations
      updateBond(cachedRequest);
      updateBond(cachedTarget);
   }

   /**
    * Constructs a bond between two lsas, via a sub-description on the first.
    * @param a_bondRequestLsa the LSA containing the bond request subdescription
    * @param a_bondSubDescription the subdescription issuing the request
    * @param the_matchedLsas the lsas to bond with
    * @param an_operationManager the operation manager to perform the operation.
    */
   public void constructUniDirectionalBondSDToLSA(Lsa a_bondRequestLsa, SubDescription a_bondSubDescription,
         Lsa a_matchedLsa) {
      // Use cached versions
      final Lsa cachedRequest = getCached(a_bondRequestLsa);
      final SubDescription cachedSubDescription = cachedRequest.getSubDescriptionByName(cachedRequest
            .getSubDescriptionName(a_bondSubDescription.getId()));
      // Add bond
      cachedSubDescription.addBond(a_matchedLsa.getId().toString());

      // Update operations
      updateBond(cachedRequest);
   }

   /**
    * Constructs a bond between two lsa SDs, .
    * @param a_bondRequestLsa the LSA containing the bond request subdescription
    * @param a_bondSubDescription the subdescription issuing the request
    * @param a_matchedSubDescription the subdescription that was matched with
    * @param an_operationManager the operation manager to perform the operation.
    */
   public void constructUniDirectionalBondSDToSD(Lsa a_bondRequestLsa, SubDescription a_bondSubDescription,
         Lsa a_matchedLsa, SubDescription a_matchedSubDescription) {
      // Use cached versions
      final Lsa cachedRequest = getCached(a_bondRequestLsa);
      final Lsa cachedTarget = getCached(a_matchedLsa);
      final SubDescription cachedSubDescription = cachedRequest.getSubDescriptionByName(cachedRequest
            .getSubDescriptionName(a_bondSubDescription.getId()));
      final SubDescription cachedTargetSubDescription = cachedTarget.getSubDescriptionByName(cachedTarget
            .getSubDescriptionName(a_matchedSubDescription.getId()));

      // Add bond
      cachedSubDescription.addBond(cachedTargetSubDescription.getId().toString());

      // Update operations
      updateBond(cachedRequest);
   }

   // TODO: This method does not work due to the intermediate/no-caching problem. Needs revised.
   // Disabled -  Use bonds from SD only for now.
   private void execBondsFromLSA() {
      for (Lsa lsa : getLSAs()) {
         Lsa candidateRequester = lsa;
         boolean continueWithSd = true;
         if (isBondRequester(candidateRequester)) {
            List<Lsa> newAllLsa = getLSAs();

            for (Lsa newLsa : newAllLsa) {
               if (lsa != newLsa) {
                  if (isBondable(newLsa)) {
                     if (candidateRequester.isFormalOne()) {
                        boolean found = candidateRequester.matches(newLsa);
                        if (found) {

                           Lsa candidateRequesterCopy = candidateRequester.getCopy();
                           candidateRequesterCopy.addBond(newLsa.getId().toString());
                           updateBondParamaterised(candidateRequesterCopy);
                           candidateRequester = candidateRequesterCopy;

                           Lsa allLsajCopy = newLsa.getCopy();
                           allLsajCopy.addBond(candidateRequesterCopy.getId().toString());
                           updateBondParamaterised(allLsajCopy);
                           continueWithSd = false;

                           break;
                        }

                     } else {
                        boolean found = candidateRequester.matches(newLsa);
                        if (found) {
                           Lsa candidateRequesterCopy = candidateRequester.getCopy();
                           candidateRequesterCopy.addBond(newLsa.getId().toString());
                           updateBond(candidateRequesterCopy);
                           candidateRequester = candidateRequesterCopy; // ////
                        }
                     }

                  }

                  SubDescription sd[] = null;
                  sd = newLsa.getSubDescriptions();
                  if (sd != null) {
                     for (int k = 0; k < sd.length; k++) {
                        if (isBondable(sd[k]) && continueWithSd) {
                           if (candidateRequester.isFormalOne()) {

                              boolean found = candidateRequester.matches(sd[k]);
                              if (found) {
                                 continueWithSd = false;
                                 Lsa candidateRequesterCopy = candidateRequester.getCopy();
                                 candidateRequesterCopy.addBond(sd[k].getId().toString());
                                 updateBond(candidateRequesterCopy);

                                 Lsa sdkLsaCopy = newLsa.getCopy();
                                 SubDescription cr = sdkLsaCopy.getSubDescriptionByName(newLsa
                                       .getSubDescriptionName(sd[k].getId()));
                                 cr.addBond(candidateRequester.getId().toString());
                                 updateBond(sdkLsaCopy);
                                 break;
                              }
                           }

                           else {

                              boolean found = candidateRequester.matches(sd[k]);
                              if (found) {
                                 Lsa candidateRequesterCopy = candidateRequester.getCopy();
                                 candidateRequesterCopy.addBond(sd[k].getId().toString());
                                 updateBond(candidateRequesterCopy);
                                 candidateRequester = candidateRequesterCopy;
                              }
                           }
                        }
                     }

                  }// end SD == null

               }// end if i!= j
            }

         }// end check LSA-LSA e LSA-SD

      }

   }

   private boolean isLsaId(String s) {
      if (s.indexOf("#") != s.lastIndexOf("#"))
         return false;
      return true;
   }

   private boolean isBondRequester(Lsa candidate) {

      if (candidate.isFormal())
         if (!(candidate.isFormalOne() && candidate.hasBonds()))
            return true;

      return false;
   }

   private boolean isBondRequester(SubDescription candidate) {
      if (candidate.isFormal())
         if (!(candidate.isFormalOne() && candidate.hasBonds()))
            return true;

      return false;
   }

   private boolean isBondable(Lsa bondCandidate) {

      if ((!bondCandidate.isFormal()) && (!bondCandidate.hasBonds()))
         return true;

      if (bondCandidate.isFormalOne() && !bondCandidate.hasBonds())
         return true;

      if (bondCandidate.isFormalMany())
         return true;

      return false;
   }

   private boolean isBondable(SubDescription bondCandidate) {

      if ((!bondCandidate.isFormal()) && (!bondCandidate.hasBonds()))
         return true;

      if (bondCandidate.isFormalOne() && !bondCandidate.hasBonds())
         return true;

      if (bondCandidate.isFormalMany())
         return true;

      return false;
   }

   /**
    * Returns the copy of this LSA from the cache if it exists, otherwise caches this Lsa and
    * returns a new copy.
    * @param an_lsa the LSA to search for in the cache.
    * @return the cached version of the lsa.
    */
   public Lsa getCached(final Lsa an_lsa) {
      if (!CACHE.containsKey(an_lsa.getId().toString())) {
         CACHE.put(an_lsa.getId().toString(), an_lsa.getCopy());
      }
      return CACHE.get(an_lsa.getId().toString());
   }

   /**
    * {@inheritDoc}
    */
   @Override
   public boolean doesSpread() {
      return false;
   }

   /**
    * {@inheritDoc}
    */
   @Override
   public boolean doesDirectPropagation() {
      return false;
   }

   /**
    * {@inheritDoc}
    */
   @Override
   public String getName() {
      return EcoLawsEngine.ECO_LAWS_AGENT;
   }

}
