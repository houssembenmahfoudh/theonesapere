/**
 * 
 */
package sapere.node.lsaspace.ecolaws.spreading;

import one.core.DTNHost;
import sapere.lsa.Lsa;
import sapere.node.NodeManager;
import extension.SapereProperties;

/**
 * A probabilistic spreading eco-law.
 * 
 * @author Graeme Stevenson (STA)
 */
public class ProbabilisticSpreading extends AbstractSpreadingEcoLaw {

	/**
	 * Creates a new instance of the probabilistic spreading eco-law.
	 * 
	 * @param a_nodeManager
	 *            the node manager of the space.
	 * @param a_dtnHost
	 *            the hosting node.
	 * @param dtnHost
	 */
	public ProbabilisticSpreading(final NodeManager a_nodeManager, final DTNHost a_dtnHost) {
		super(a_nodeManager, a_dtnHost);
	}

	/**
	 * Whether this LSA should be spread.
	 * 
	 * @throws InterruptedException
	 */
	public boolean shouldLsaBeSpread(final Lsa lsa) {
		if (isSpreadable(lsa) && decayIsMaximum(lsa) && hasNotAlreadySpread(lsa)) {
			return true;
		} else {
			return isSpreadable(lsa) && hasNotAlreadySpread(lsa) && probabilityTestPassed();
		}

	}

	/**
	 * Assesses whether or not to spread based on the specified global
	 * probability value.
	 * 
	 * @return true if spreading should take place, false otherwise.
	 */
	private boolean probabilityTestPassed() {
		boolean probabilityTest = SapereProperties.RANDOM.nextDouble() < SapereProperties.SPREADING_PROBABILITY;
		// System.out.println(SimClock.getTime() + ": " + getHost().getName()
		// + " send? " + probabilityTest);
		return probabilityTest;
	}

	/**
	 * Add any required pre-processing on the copy of the LSA to be sent.
	 * 
	 * @param an_lsa
	 *            the clone of the local LSA that will be sent.
	 */
	@Override
	public void beforeSpreading(Lsa an_lsa) {
		// No preprocessing required.
	}

}
