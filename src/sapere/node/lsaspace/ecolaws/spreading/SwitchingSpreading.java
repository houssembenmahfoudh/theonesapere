/**
 * 
 */
package sapere.node.lsaspace.ecolaws.spreading;

import java.util.List;

import extension.SapereProperties;

import one.core.Coord;
import one.core.DTNHost;
import sapere.lsa.Lsa;
import sapere.node.NodeManager;

/**
 * A location-based spreading eco-law.
 * 
 * @author Graeme Stevenson (STA)
 */
public class SwitchingSpreading extends AbstractSpreadingEcoLaw {
	
	/**
	 * The currently selected spreading scheme.
	 */
	private AbstractSpreadingEcoLaw my_activeScheme;
	
	/**
	 * An instance of the adaptive spreading scheme.
	 */
	private final AdaptiveProbabilisticSpreading my_probabilisticScheme;
	
	/**
	 * An instance of the location spreading scheme.
	 */
	private final LocationBasedSpreading my_locationScheme;

	/**
	 * Creates a new instance of the switching spreading eco-law.
	 * 
	 * @param a_nodeManager
	 *            the node manager of the space.
	 * @param a_dtnHost
	 *            the hosting node.
	 */
	public SwitchingSpreading(final NodeManager a_nodeManager,
			final DTNHost a_dtnHost) {
		super(a_nodeManager, a_dtnHost);
		my_probabilisticScheme = new AdaptiveProbabilisticSpreading(
				a_nodeManager, a_dtnHost);
		my_locationScheme = new LocationBasedSpreading(a_nodeManager, a_dtnHost);
	}

	/**
	 * Whether this LSA should be spread.
	 */
	public boolean shouldLsaBeSpread(final Lsa lsa) {
		// First, we check which scheme we should be using.
		// To use location based spreading we require:
		// (1) The node to be GPS enabled; and,
		// (2) The LSA to have more than 2 source coordinates.
		// Otherwise, we use probabilistic spreading.

		// Get the coordinate list from the LSA
		List<Coord> coordinateList = my_locationScheme
				.extractCoordinateListFromLsa(lsa);

		// Test to see if the criteria for using location based are met.
		if (getNodeManager().isGPSEnabled() && coordinateList.size() >= 2) {
			my_activeScheme = my_locationScheme;
			SapereProperties.location++;
		} else {
			my_activeScheme = my_probabilisticScheme;
			SapereProperties.prob++;
		}
		// Return the call applied to the selected scheme.
		// Check first that the LSA has not already been spread;
		return hasNotAlreadySpread(lsa) && my_activeScheme.shouldLsaBeSpread(lsa);
	}

	@Override
	void beforeSpreading(Lsa lsaCopy) {
		my_activeScheme.beforeSpreading(lsaCopy);
	}
}
