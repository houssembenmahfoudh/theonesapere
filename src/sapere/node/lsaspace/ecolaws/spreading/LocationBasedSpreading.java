/**
 * 
 */
package sapere.node.lsaspace.ecolaws.spreading;

import java.awt.Polygon;
import java.awt.geom.Line2D;
import java.util.LinkedList;
import java.util.List;
import one.core.Coord;
import one.core.DTNHost;
import org.paukov.combinatorics.Factory;
import org.paukov.combinatorics.Generator;
import org.paukov.combinatorics.ICombinatoricsVector;
import extension.SapereProperties;
import sapere.lsa.Lsa;
import sapere.lsa.Property;
import sapere.node.NodeManager;

/**
 * A location-based spreading eco-law.
 * 
 * @author Graeme Stevenson (STA)
 */
public class LocationBasedSpreading extends AbstractSpreadingEcoLaw {

	/**
	 * Distance settings that control re-broadcasting - should be configured to
	 * match the communication radius.
	 */
	public static final double DISTANCE_TO_POINT_THRESHOLD = SapereProperties.LOCATION_THRESHOLD;

	/**
	 * Distance settings that control re-broadcasting - should be configured to
	 * match the communication radius.
	 */
	public static final double DISTANCE_TO_LINE_THRESHOLD = SapereProperties.LOCATION_THRESHOLD;

	/**
	 * Creates a new instance of the location based spreading eco-law.
	 * 
	 * @param a_nodeManager
	 *            the node manager of the space.
	 * @param a_dtnHost
	 *            the hosting node.
	 * @param dtnHost
	 */
	public LocationBasedSpreading(final NodeManager a_nodeManager, final DTNHost a_dtnHost) {
		super(a_nodeManager, a_dtnHost);
	}

	/**
	 * Whether this LSA should be spread.
	 */
	public boolean shouldLsaBeSpread(final Lsa lsa) {
		return isSpreadable(lsa) && hasNotAlreadySpread(lsa) && hostCanProvideAdditionalCoverage(lsa);
	}

	/**
	 * Decide whether or not to spread the LSA from this host based on assessed
	 * additional coverage it can provide.
	 * 
	 * @param a_lsa
	 *            the lsa to inspect.
	 * @return true if this host should spread based on location, false
	 *         otherwise.
	 */
	//
	private boolean hostCanProvideAdditionalCoverage(final Lsa a_lsa) {
		List<Coord> coordinateList = extractCoordinateListFromLsa(a_lsa);
		switch (coordinateList.size()) {
		case 0:
			// LSA originated here, always spread.
			// System.out.println("Origin node - spreading");
			return true;
		case 1:
			return shouldSpreadWithOneSource(coordinateList.get(0));
		case 2:
			return shouldSpreadWithTwoSources(coordinateList.get(0), coordinateList.get(1));
		default:
			return shouldSpreadWithThreeOrMoreSources(coordinateList);
		}
	}

	/**
	 * Should we rebroadcast an LSA received from a single source. Decision is
	 * made based on distance.
	 * 
	 * @param coordinate
	 *            the source coordinate.
	 * @return true if we should rebroadcast, false otherwise.
	 */
	private boolean shouldSpreadWithOneSource(Coord coordinate) {
		// Base decision based on distance to wireless radius
		double distance = getHost().getLocation().distance(coordinate);
		// System.out.print(SimClock.getTime() + ": " +getHost().getName()
		// + " - LSA received from 1 source, distance: " + distance + ", ");
		// Make a re-broadcast decision based on distance to the source.
		if (DISTANCE_TO_POINT_THRESHOLD < distance) {
			// System.out.println("rebroadcasting");
			return true;
		} else {
			// System.out.println("not rebroadcasting");
			return false;
		}
	}

	/**
	 * Should we rebroadcast an LSA received from two sources. Decision is made
	 * based on distance to the line connecting the two sources.
	 * 
	 * @param coordinateA
	 *            the source coordinate A.
	 * 
	 * @param coordinateB
	 *            the source coordinate B.
	 * @return true if we should rebroadcast, false otherwise.
	 */
	private boolean shouldSpreadWithTwoSources(Coord coordinateA, Coord coordinateB) {
		// Create the line connecting the two sources.
		Line2D.Double line = new Line2D.Double(coordinateA.getX(), coordinateA.getY(), coordinateB.getX(),
				coordinateB.getY());
		// Calculate the distance of this point to the line.
		double distanceToLine = line.ptLineDist(getHost().getLocation().getX(), getHost().getLocation().getY());
		// System.out.print(SimClock.getTime() + ": " +getHost().getName()
		// + " - LSA received from 2, distance to connecting line: "
		// + distanceToLine + ", ");
		// Make a re-broadcast decision based on distance to the line.
		if (DISTANCE_TO_LINE_THRESHOLD < distanceToLine) {
			// System.out.println("rebroadcasting");
			return true;
		} else {
			// System.out.println("not rebroadcasting");
			return false;
		}
	}

	/**
	 * Should we rebroadcast an LSA received from four or more sources.
	 * 
	 * @param coordinates
	 *            the source coordinates.
	 * @return true if we should, false otherwise.
	 */
	private boolean shouldSpreadWithThreeOrMoreSources(List<Coord> coordinates) {
		if (coordinates.size() < 3) {
			throw new IllegalArgumentException("Expected four or more coordinates as input");
		}
		// System.out.print(SimClock.getTime() + ": " +getHost().getName() + "
		// - LSA received from "
		// + coordinates.size() + " sources, point in polygon? ");
		// The approach is to deconstruct the polygon into all possible
		// triangles. If the local host is position in any of the triangles, we
		// do not need to rebroadcast.

		// Create a combination generator to generate 3-combinations
		final ICombinatoricsVector<Coord> initialVector = Factory.createVector(coordinates);
		final Generator<Coord> generator = Factory.createSimpleCombinationGenerator(initialVector, 3);

		// Iterate through all possible combinations until one is found
		for (ICombinatoricsVector<Coord> combination : generator) {
			if (pointInTriangle(combination)) {
				// System.out.println("true, not rebroadcasting");
				return false;
			}
		}
		// combinations are exhausted, point not in triangle.
		// System.out.println("false, rebroadcasting");
		return true;
	}

	/**
	 * Should we rebroadcast an LSA received from three sources.
	 * 
	 * @param coordinates
	 *            the source coordinates.
	 * @return true if we should, false otherwise.
	 */
	private boolean pointInTriangle(Iterable<Coord> coordinates) {
		// Construct the triangle
		Polygon triangle = new Polygon();
		for (Coord coordinate : coordinates) {
			triangle.addPoint(Double.valueOf(coordinate.getX()).intValue(),
					Double.valueOf(coordinate.getY()).intValue());
		}

		return triangle.contains(getHost().getLocation().getX(), getHost().getLocation().getY());
	}

	/**
	 * Extracts the set of source coordinates from an LSA.
	 * 
	 * @param an_lsa
	 *            the LSA to extract the data from.
	 * @return the list of coordinated from which an LSA was spread.
	 */
	List<Coord> extractCoordinateListFromLsa(final Lsa an_lsa) {
		// Get a list of known spreading coordinates
		List<String> stringCoordinateList = new LinkedList<String>();
		if (an_lsa.hasProperty("spread_from")) {
			stringCoordinateList.addAll(an_lsa.getProperty("spread_from").getValue());
		}

		// Choose strategy based on the number of known spreading locations
		List<Coord> coordinateList = toCoordinateList(stringCoordinateList);
		return coordinateList;
	}

	/**
	 * Converts a list of coordinated represented as strings to proper Coord
	 * objects.
	 * 
	 * @param stringCoordinateList
	 *            the list of coordinates in string form.
	 * @return the converted list of objects.
	 */
	private List<Coord> toCoordinateList(List<String> stringCoordinateList) {
		List<Coord> coordinateList = new LinkedList<Coord>();
		for (String string : stringCoordinateList) {
			Coord coordinate = new Coord(string);
			coordinateList.add(coordinate);
		}
		return coordinateList;
	}

	/**
	 * Adds a spread_from property containing the coordinates of this space.
	 * 
	 * @param an_lsa
	 *            the LSA to tag.
	 */
	@Override
	public void beforeSpreading(Lsa an_lsa) {
		if (getNodeManager().isGPSEnabled()) {
			an_lsa.removeProperty("spread_from");
			an_lsa.addProperty(new Property("spread_from", getHost().getLocation().toString()));
		}
	}

}
