package sapere.node.lsaspace.ecolaws;

import java.util.List;
import java.util.Vector;
import sapere.lsa.Id;
import sapere.lsa.Lsa;
import sapere.lsa.Property;
import sapere.lsa.exception.UnresolvedPropertyNameException;
import sapere.lsa.values.PropertyName;
import sapere.node.NodeManager;
import sapere.node.lsaspace.EcoLawsEngine;

/**
 * @author Gabriella Castelli (UNIMORE)
 * @author Graeme Stevenson (STA)
 */
public class Aggregation extends AbstractEcoLaw {

	/*
	 * LSA that wants to be propagated MUST have the following fields defined in
	 * PropertyValue: aggregation_op: Aggregation Operators field_value: Field
	 * to which the Aggregation Operator will be applied source: an id to
	 * identify the LSAs to be aggregated together
	 */

	/**
	 * Creates a new instance of the aggregation eco-law.
	 * 
	 * @param a_nodeManager
	 *            the node manager of the space.
	 */
	public Aggregation(final NodeManager a_nodeManager) {
		super(a_nodeManager);
	}

	public void invoke() {
		selfAggregation();
		otherAggregation();
	}

	private void selfAggregation() {
		Vector<String> sourceProcessed = new Vector<String>();
		Vector<Lsa> toRemove = new Vector<Lsa>();
		List<Lsa> lsas = getLSAs();
		for (Lsa lsa : lsas) {

			Lsa lsa2 = lsa;
			if (lsa.explicitAggregationApplies() && !sourceProcessed.contains(lsa.getAggregationSource().toString())) {
				sourceProcessed.add(lsa.getAggregationSource().toString());

				Vector<Lsa> compatible = getAggregationCompatibleLsaI(lsa, getLSAs());

				// EDIT BY GRAEME - prevents aggregation when only a single LSA
				// is present. (i.e.,
				// aggregation with itself).
				if (compatible.size() >= 2) {

					if (lsa.getAggregationOp().equals(AggregationOperators.MAX.toString())) {
						Id l = null;
						l = AggregationOperators.max(compatible);

						if (l != null) {
							if (!lsa.getId().toString().equals(l.toString()))
								toRemove.add(lsa);

							for (int k = 0; k < compatible.size(); k++)
								if (!compatible.elementAt(k).getId().toString().equals(l.toString()))
									toRemove.add(compatible.elementAt(k));
						}

					}

					if (lsa.getAggregationOp().equals(AggregationOperators.MIN.toString())) {
						Id l = null;
						l = AggregationOperators.min(compatible);

						if (l != null) {
							if (!lsa.getId().toString().equals(l.toString()))
								toRemove.add(lsa);

							for (int k = 0; k < compatible.size(); k++)
								if (!compatible.elementAt(k).getId().toString().equals(l.toString()))
									toRemove.add(compatible.elementAt(k));
						}

					}

					if (lsa.getAggregationOp().equals(AggregationOperators.AVG.toString())) {
						// compatible.add(allLsa[i]);

						Lsa l = AggregationOperators.avg(compatible).getCopy();

						for (int k = 0; k < compatible.size(); k++) {
							toRemove.add(compatible.elementAt(k));
						}
						inject(l);
					}
				} // end if compatible.size >2

			}

		}

		for (int j = 0; j < toRemove.size(); j++) {
			remove(toRemove.elementAt(j));
		}
	}

	private void otherAggregation() {

		for (Lsa lsa : getLSAs()) {

			Lsa lsa2 = lsa;
			if (lsa.requestedAggregationApplies()) {

				Vector<Lsa> compatible = getRequestAggregationCompatibleLsa(lsa, getLSAs());

				if (lsa.getAggregationOp().equals(AggregationOperators.MAX.toString())) {

					/*
					 * l = AggregationOperators.max(compatible,
					 * allLsa[i].getProperty(PropertyName.FIELD_VALUE
					 * .toString()).getValue().elementAt(0)); l.addProperty(new
					 * Property("aggregated_by",
					 * AggregationOperators.MAX.toString()));
					 */

					String pName = lsa.getProperty(PropertyName.FIELD_VALUE.toString()).getValue().elementAt(0);
					String value = AggregationOperators.maxValue(compatible,
							lsa.getProperty(PropertyName.FIELD_VALUE.toString()).getValue().elementAt(0));
					if (lsa.hasProperty(pName))
						try {
							lsa.setProperty(new Property(pName, value));
						} catch (UnresolvedPropertyNameException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					else
						lsa.addProperty(new Property(pName, value));

				}

				if (lsa.getAggregationOp().equals(AggregationOperators.MIN.toString())) {

					// l = AggregationOperators.min(compatible,
					// allLsa[i].getProperty(PropertyName.FIELD_VALUE.toString()).getValue().elementAt(0));
					// l.addProperty(new Property("aggregated_by",
					// AggregationOperators.MIN.toString()));

					String pName = lsa.getProperty(PropertyName.FIELD_VALUE.toString()).getValue().elementAt(0);
					String value = AggregationOperators.minValue(compatible,
							lsa.getProperty(PropertyName.FIELD_VALUE.toString()).getValue().elementAt(0));
					if (lsa.hasProperty(pName))
						try {
							lsa.setProperty(new Property(pName, value));
						} catch (UnresolvedPropertyNameException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					else
						lsa.addProperty(new Property(pName, value));
				}

				if (lsa.getAggregationOp().equals(AggregationOperators.AVG.toString())) {

					// l = AggregationOperators.mean(compatible,
					// allLsa[i].getProperty(PropertyName.FIELD_VALUE.toString()).getValue().elementAt(0)).getCopy();
					// l.addProperty(new Property("aggregated_by",
					// AggregationOperators.AVG.toString()));
					String pName = lsa.getProperty(PropertyName.FIELD_VALUE.toString()).getValue().elementAt(0);
					String value = AggregationOperators.avgValue(compatible,
							lsa.getProperty(PropertyName.FIELD_VALUE.toString()).getValue().elementAt(0));
					if (lsa.hasProperty(pName))
						try {
							lsa.setProperty(new Property(pName, value));
						} catch (UnresolvedPropertyNameException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					else
						lsa.addProperty(new Property(pName, value));
				}

				// Operation inject = new Operation().injectOperation(l,
				// EcoLawsEngine.ECO_LAWS_AGGREGATION, null);
				// opMng.execOperation(inject);

			}

		}

	}

	public Vector<Lsa> getRequestAggregationCompatibleLsa(Lsa lsa, List<Lsa> allLsa) {
		Vector<Lsa> ret = new Vector<Lsa>();

		for (Lsa secondLsa : allLsa) {

			if (!secondLsa.requestedAggregationApplies() && !secondLsa.explicitAggregationApplies()
					&& !secondLsa.hasProperty("aggregated_by") && secondLsa
							.hasProperty(lsa.getProperty(PropertyName.FIELD_VALUE.toString()).getValue().elementAt(0)))
				ret.add(secondLsa.getCopy());
		}
		return ret;
	}

	public Vector<Lsa> getAggregationCompatibleLsa(Lsa lsa, List<Lsa> allLsa) {
		Vector<Lsa> ret = new Vector<Lsa>();
		for (Lsa secondLsa : allLsa) {

			if (secondLsa.aggregationApplies() && !secondLsa.getId().toString().equals(lsa.getId().toString())
					&& secondLsa.getAggregationSource().equals(lsa.getAggregationSource()))
				ret.add(secondLsa.getCopy());
		}
		return ret;
	}

	// lsa included
	public Vector<Lsa> getAggregationCompatibleLsaI(Lsa lsa, List<Lsa> allLsa) {
		Vector<Lsa> ret = new Vector<Lsa>();
		for (Lsa secondLsa : allLsa) {

			if (secondLsa.aggregationApplies() && !secondLsa.getId().toString().equals(lsa.getId().toString())
					&& secondLsa.getAggregationSource().equals(lsa.getAggregationSource()))
				ret.add(secondLsa.getCopy());
		}

		ret.add(lsa);
		return ret;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean doesSpread() {
		return false;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean doesDirectPropagation() {
		return false;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String getName() {
		return EcoLawsEngine.ECO_LAWS_AGGREGATION;
	}

}
