/**
 * 
 */
package sapere.node.lsaspace;

import java.util.Iterator;
import java.util.Queue;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.logging.Logger;
import sapere.lsa.Id;
import sapere.lsa.Lsa;
import sapere.node.notifier.Notifier;
import sapere.node.notifier.Subscription;
import sapere.node.notifier.event.BondAddedEvent;
import sapere.node.notifier.event.BondRemovedEvent;
import sapere.node.notifier.event.DecayedEvent;
import sapere.node.notifier.event.PropagationEvent;
import sapere.node.notifier.event.ReadEvent;
import sapere.node.notifier.filter.BondAddedFilter;
import sapere.node.notifier.filter.BondRemovedFilter;
import sapere.node.notifier.filter.DecayedFilter;
import sapere.node.notifier.filter.PropagationFilter;
import sapere.node.notifier.filter.ReadFilter;
import sapere.node.util.SystemConfiguration;

import com.google.common.base.Optional;

/**
 * @author Gabriella Castelli (UNIMORE)
 */
public class OperationManager implements Runnable {

	private Queue<Operation> operationsQueue = null; // FIFO Queues operation
														// requested by
														// SapereAgents

	private Space space = null;

	private Notifier notifier = null;

	private EcoLawsEngine ecoLawsEngine = null;

	private String spaceName = null;

	private long opTime = 5000; // initialized in conf/settings.properties

	private long sleepTime = 1; // initialized in conf/settings.properties

	Logger log = null;

	public String getSpaceName() {
		return spaceName;
	}

	public OperationManager(Space space, Notifier notifier, String spaceName) {

		log = SystemConfiguration.getInstance().getLog();

		this.spaceName = spaceName;
		this.operationsQueue = new ConcurrentLinkedQueue<Operation>(); // This
																		// FIFO
																		// queue
																		// is
																		// synchronized
																		// AND
																		// NON
																		// BLOCKING
		this.space = space;
		this.notifier = notifier;

	}

	public OperationManager(Space space, Notifier notifier, String spaceName, long opTime, long sleepTime) {

		log = SystemConfiguration.getInstance().getLog();

		this.spaceName = spaceName;
		this.operationsQueue = new ConcurrentLinkedQueue<Operation>(); // This
																		// FIFO
																		// queue
																		// is
																		// synchronized
																		// AND
																		// NON
																		// BLOCKING
		this.space = space;
		this.notifier = notifier;
		this.opTime = opTime;
		this.sleepTime = sleepTime;

	}

	public void setEcoLawsEngine(EcoLawsEngine ecoLawsEngine) {
		this.ecoLawsEngine = ecoLawsEngine;
	}

	public Id execOperation(Operation operation) {
		Id id = null;

		if (operation.getOpType() == OperationType.Inject) {
			id = space.getFreshId();
			operation.setLsaId(id);

			if (operation.getRequestingAgent() != null) {

				// Subscription to BondAddedEvent
				BondAddedEvent event1 = new BondAddedEvent(null, null, null);
				BondAddedFilter filter1 = new BondAddedFilter(operation.getLsaId(), operation.getRequestingId());
				Subscription s1 = new Subscription(event1.getClass(), filter1, operation.getRequestingAgent(),
						operation.getRequestingId());
				notifier.subscribe(s1);

			}

			if (operation.getRequestingAgent() != null) {
				BondRemovedEvent event3 = new BondRemovedEvent(null, null);
				BondRemovedFilter filter3 = new BondRemovedFilter(operation.getLsaId(), operation.getRequestingId());
				Subscription s3 = new Subscription(event3.getClass(), filter3, operation.getRequestingAgent(),
						operation.getRequestingId());
				notifier.subscribe(s3);
			}

		}
		this.execOp(operation);

		return id;
	}

	public Id queueOperation(Operation operation) {
		Id id = null;

		if (operation.getOpType() == OperationType.Inject) {
			id = space.getFreshId();
			operation.setLsaId(id);

			if (operation.getLsa().hasDecayProperty()) {

				DecayedEvent event = new DecayedEvent(null);
				DecayedFilter filter = new DecayedFilter(operation.getLsaId(), operation.getRequestingId());
				Subscription s = new Subscription(event.getClass(), filter, operation.getRequestingAgent(),
						operation.getRequestingId());
				notifier.subscribe(s);
			}

			BondAddedEvent event1 = new BondAddedEvent(null, null, null);
			BondAddedFilter filter1 = new BondAddedFilter(operation.getLsaId(), operation.getRequestingId());
			Subscription s1 = new Subscription(event1.getClass(), filter1, operation.getRequestingAgent(),
					operation.getRequestingId());
			notifier.subscribe(s1);

			BondRemovedEvent event3 = new BondRemovedEvent(null, null);
			BondRemovedFilter filter3 = new BondRemovedFilter(operation.getLsaId(), operation.getRequestingId());
			Subscription s3 = new Subscription(event3.getClass(), filter3, operation.getRequestingAgent(),
					operation.getRequestingId());
			notifier.subscribe(s3);

			// Subscription to PropagationEvent
			PropagationEvent event11 = new PropagationEvent(null);
			PropagationFilter filter11 = new PropagationFilter(operation.getLsaId());
			Subscription s11 = new Subscription(event11.getClass(), filter11, operation.getRequestingAgent(),
					operation.getRequestingId());
			notifier.subscribe(s11);

		}

		if (operation.getOpType() == OperationType.Update) {
			if (operation.getLsa().hasDecayProperty()) {
				DecayedEvent event = new DecayedEvent(null);
				DecayedFilter filter = new DecayedFilter(operation.getLsaId(), operation.getRequestingId());
				Subscription s = new Subscription(event.getClass(), filter, operation.getRequestingAgent(),
						operation.getRequestingId());
				notifier.subscribe(s);
			}
		}

		// Adds the operation to the queue
		operationsQueue.add(operation);

		return id;
	}

	public void run() {
		log.finer("Starting Operation Manager... ");
		boolean operationExecuted = false;
		// Loop until there are no scheduled operations to execute.
		do {
			operationExecuted = execNextOp();
		} while (operationExecuted);

		log.finer("Stopping Operation Manager... ");

	}

	private void execOp(Operation nextOp) {

		log.fine(nextOp.toString());

		if (nextOp.getOpType() == OperationType.Inject) {
			// Lsa lsa = nextOp.getLsa();
			// Property p =
			// lsa.getProperty(PropertyName.GRADIENT_PREVIOUS.toString());
			// if (p != null)
			// {
			// String from = p.getValue().firstElement();
			//
			// if (this.spaceName.equals("B1") && from.equals("A0"))
			// {
			// System.out.println("Injecting");
			// try {
			// System.in.read();
			// } catch (IOException e) {
			// // TODO Auto-generated catch block
			// e.printStackTrace();
			// }
			// }
			// }
			space.inject(nextOp.getLsa(), nextOp.getRequestingId());
		}
		if (nextOp.getOpType() == OperationType.Remove)
			space.remove(nextOp.getLsaId(), nextOp.getRequestingId());
		if (nextOp.getOpType() == OperationType.Read) {

			ReadEvent event = new ReadEvent(null);
			ReadFilter filter = new ReadFilter(nextOp.getLsaId(), nextOp.getRequestingId());
			Subscription s = new Subscription(event.getClass(), filter, nextOp.getRequestingAgent(),
					nextOp.getRequestingId());

			notifier.subscribe(s);

			space.read(nextOp.getLsaId(), nextOp.getRequestingId()); // TO DO: e
																		// return
																		// value
																		// is
																		// needed

			notifier.unsubscribe(s);

		}

		// TO DO: check if needed
		if (nextOp.getOpType() == OperationType.Update)
			space.update(nextOp.getLsaId(), nextOp.getLsa(), nextOp.getRequestingId());
		if (nextOp.getOpType() == OperationType.BondUpdate)
			space.update(nextOp.getLsaId(), nextOp.getLsa(), nextOp.getRequestingId(), true, false); // true,
																										// false
		if (nextOp.getOpType() == OperationType.UpdateParametrized)
			space.update(nextOp.getLsaId(), nextOp.getLsa(), nextOp.getRequestingId(), false, true);
		if (nextOp.getOpType() == OperationType.BondUpdateParametrized)
			space.update(nextOp.getLsaId(), nextOp.getLsa(), nextOp.getRequestingId(), false, false); // false,
																										// false

	}

	private boolean execNextOp() {
		Operation nextOp = null;
		boolean ret = false;

		Iterator<Operation> iterator = operationsQueue.iterator();

		if (iterator.hasNext()) {
			nextOp = (Operation) iterator.next();
			operationsQueue.poll();
			execOp(nextOp);
			ret = true;
		} else
			return false;

		return ret;
	}

	/**
	 * Returns a copy of an LSA. This method should enforce security to ensure
	 * an LSA may only read LSAs to which it is directly or indirectly bound. It
	 * currently serves as an alternative to the read operation for the purpose
	 * of simplifying application development that involves reading indirect
	 * bonds.
	 * 
	 * @param an_id
	 *            the id of the LSA to read.
	 * @return an Optional containing a copy of the LSA with the requested ID.
	 */
	public Optional<Lsa> readLSACopy(final String an_id) {
		final Optional<Lsa> lookup = Optional.fromNullable(space.getLsa(an_id));
		if (lookup.isPresent()) {
			return Optional.fromNullable(lookup.get().getCopy());
		} else {
			return Optional.absent();
		}

	}

}
