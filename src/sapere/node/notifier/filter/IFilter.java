package sapere.node.notifier.filter;

import sapere.node.notifier.event.IEvent;

/**
 * @author Gabriella Castelli (UNIMORE)
 *
 */
public interface IFilter {
  public boolean apply(IEvent event);
  public boolean apply(IEvent event, String agentName);
}