/**
 * 
 */
package sapere.node.notifier.filter;


import sapere.lsa.Id;
import sapere.lsa.Lsa;
import sapere.node.notifier.event.BondAddedEvent;
import sapere.node.notifier.event.IEvent;



/**
 * @author Gabriella Castelli (UNIMORE)
 *
 */


public class BondAddedFilter implements IFilter{
	
	private Id targetLsaId = null;
	private String requestingId = null;
	
	public BondAddedFilter(Id lsaId, String requestingId){
		this.targetLsaId = lsaId;
		this.requestingId = requestingId;
	}

	
	public boolean apply(IEvent event) {
		
		boolean ret = false;
		
	    BondAddedEvent bondAddedEvent = (BondAddedEvent) event;
	    
	    if ( ((Lsa) bondAddedEvent.getLsa()).getId().toString().equals(targetLsaId.toString()) && bondAddedEvent.getRequiringAgent().equals(requestingId))
	    	ret = true;
		
		return ret;
	}


	public boolean apply(IEvent event, String lsaSubscriberId) {
		// TODO Auto-generated method stub
		return false;
	}
	
	public boolean equals (Object o){
		boolean ret = false; 
		if (o instanceof BondAddedFilter){
		ret = ( targetLsaId.toString().equals( ((BondAddedFilter) o).targetLsaId.toString()) &&
				requestingId.equals(((BondAddedFilter) o).requestingId));
		}
		return ret;
	}

}
