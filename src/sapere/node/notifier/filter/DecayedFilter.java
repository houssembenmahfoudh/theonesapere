/**
 * 
 */
package sapere.node.notifier.filter;


import sapere.lsa.Id;
import sapere.node.notifier.event.DecayedEvent;
import sapere.node.notifier.event.IEvent;



/**
 * @author Gabriella Castelli (UNIMORE)
 *
 */


public class DecayedFilter implements IFilter{
	
	private Id targetLsaId = null;
	private String requestingId = null;
	
	public DecayedFilter(Id lsaId, String requestingId){
		this.targetLsaId = lsaId;
		this.requestingId = requestingId;
	}

	
	public boolean apply(IEvent event) {
		
		boolean ret = false;
		
	    DecayedEvent decayEvent = (DecayedEvent) event;
	    
	    if (decayEvent.getLsa().getId().toString().equals(targetLsaId.toString()) )
	    	ret = true;
		
		return ret;
	}


	public boolean apply(IEvent event, String lsaSubscriberId) {
		// TODO Auto-generated method stub
		return false;
	}
	
	public boolean equals (Object o){
		boolean ret = false; 
		if (o instanceof DecayedFilter){
		ret = ( targetLsaId.toString().equals( ((DecayedFilter) o).targetLsaId.toString()) &&
				requestingId.equals(((DecayedFilter) o).requestingId));
		}
		return ret;
	}

}
