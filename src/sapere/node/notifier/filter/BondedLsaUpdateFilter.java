/**
 * 
 */
package sapere.node.notifier.filter;

import sapere.lsa.Lsa;
import sapere.lsa.Id;
import sapere.node.notifier.event.BondedLsaUpdateEvent;
import sapere.node.notifier.event.IEvent;



/**
 * @author Gabriella Castelli (UNIMORE)
 *
 */


public class BondedLsaUpdateFilter implements IFilter{
	
	private Id targetLsaId = null;
	
	public Id getTargetLsaId() {
		return targetLsaId;
	}


	public void setTargetLsaId(Id targetLsaId) {
		this.targetLsaId = targetLsaId;
	}


	public String getRequestingId() {
		return requestingId;
	}


	public void setRequestingId(String requestingId) {
		this.requestingId = requestingId;
	}


	private String requestingId = null;
	
	public BondedLsaUpdateFilter(Id lsaId, String requestingId){
		this.targetLsaId = lsaId; // changedId
		this.requestingId = requestingId; //requesting Id
		
	
	}

	
	public boolean apply(IEvent event, String agentName) {
		
		boolean ret = false;
		
		BondedLsaUpdateEvent bondedLsaUpdateEvent = (BondedLsaUpdateEvent) event;
		
		//System.out.println("Agent: " + agentName);
		if ((Lsa)bondedLsaUpdateEvent.getLsa() == null)
			System.out.println("NULLO");
		if (targetLsaId == null)
			System.out.println("NULLO2");
		if (((Lsa)bondedLsaUpdateEvent.getLsa()).getId() == null)
			return false;
		//System.out.println((Lsa)bondedLsaUpdateEvent.getLsa());
		 if (((Lsa)bondedLsaUpdateEvent.getLsa()).getId().toString().equals(targetLsaId.toString()) )
				 ret = true;
		
		return ret;
	}


	public boolean apply(IEvent event) {
		// TODO Auto-generated method stub
		return false;
	}
	
	public boolean equals (Object o){
		boolean ret = false; 
		if (o instanceof BondedLsaUpdateFilter){
		ret = ( targetLsaId.toString().equals( ((BondedLsaUpdateFilter) o).targetLsaId.toString()) &&
				requestingId.equals(((BondedLsaUpdateFilter) o).requestingId));
		}
		return ret;
	}


}
