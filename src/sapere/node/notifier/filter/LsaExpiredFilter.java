/**
 * 
 */
package sapere.node.notifier.filter;

import sapere.lsa.Id;
import sapere.node.notifier.event.IEvent;
import sapere.node.notifier.event.LsaExpiredEvent;



/**
 * @author Gabriella Castelli (UNIMORE)
 *
 */


public class LsaExpiredFilter implements IFilter{
	
	private Id targetLsaId = null;
	private String requestingId = null;
	
	public LsaExpiredFilter(Id lsaId, String requestingId){
		this.targetLsaId = lsaId;
		this.requestingId = requestingId;
	}

	
	public boolean apply(IEvent event) {
		
		boolean ret = false;
		
		LsaExpiredEvent lsaExpiredEvent = (LsaExpiredEvent) event;
	    
	    if (lsaExpiredEvent.getLsa().getId().toString().equals(targetLsaId.toString()) && lsaExpiredEvent.getRequiringAgent().equals(requestingId))
	    	ret = true;
		
		return ret;
	}


	public boolean apply(IEvent event, String lsaSubscriberId) {
		// TODO Auto-generated method stub
		return false;
	}
	
	public boolean equals (Object o){
		boolean ret = false; 
		if (o instanceof LsaExpiredFilter){
		ret = ( targetLsaId.toString().equals( ((LsaExpiredFilter) o).targetLsaId.toString()) &&
				requestingId.equals(((LsaExpiredFilter) o).requestingId));
		}
		return ret;
	}

}
