/**
 * 
 */
package sapere.node.notifier.filter;

import sapere.lsa.Lsa;
import sapere.lsa.Id;
import sapere.node.notifier.event.BondRemovedEvent;
import sapere.node.notifier.event.IEvent;



/**
 * @author Gabriella Castelli (UNIMORE)
 *
 */


public class BondRemovedFilter implements IFilter{
	 
	private Id targetLsaId = null;
	private String requestingId = null;
	
	public BondRemovedFilter(Id lsaId, String requestingId){
		this.targetLsaId = lsaId;
		this.requestingId = requestingId;
	}

	
	public boolean apply(IEvent event) {
		
		boolean ret = false;
		
	    BondRemovedEvent bondRemovedEvent = (BondRemovedEvent) event;
	    
	    if (((Lsa)bondRemovedEvent.getLsa()).getId().toString().equals(targetLsaId.toString()) && bondRemovedEvent.getRequiringAgent().equals(requestingId))
	    	ret = true;
		
		return ret;
	}


	public boolean apply(IEvent event, String lsaSubscriberId) {
		// TODO Auto-generated method stub
		return false;
	}
	
	public boolean equals (Object o){
		boolean ret = false; 
		if (o instanceof BondRemovedFilter){
		ret = ( targetLsaId.toString().equals( ((BondRemovedFilter) o).targetLsaId.toString()) &&
				requestingId.equals(((BondRemovedFilter) o).requestingId));
		}
		return ret;
	}

}
