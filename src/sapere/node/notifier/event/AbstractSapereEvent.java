package sapere.node.notifier.event;

import java.io.Serializable;


/**
 * 
 */

/**
 * @author Gabriella Castelli (UNIMORE)
 *
 */
public abstract class AbstractSapereEvent implements IEvent, Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1037415768070620266L;
	private String requiringAgent = null;
	
	/**
	 * @return the requiringAgent
	 */
	public String getRequiringAgent() {
		return requiringAgent;
	}


	/**
	 * @param requiringAgent the requiringAgent to set
	 */
	public void setRequiringAgent(String requiringAgent) {
		this.requiringAgent = requiringAgent;
	}
	
}

