/**
 * 
 */
package sapere.node.notifier.event;

import sapere.lsa.Lsa;
import sapere.lsa.interfaces.ILsa;


/**
 * @author Gabriella Castelli (UNIMORE)
 *
 */
public class BondAddedEvent extends AbstractSapereEvent{
	
	private Lsa lsa = null;
	private Lsa bondedLsa = null;
	private String bondId = null; // the the Id of the LSa to which I have just been bonded

	public BondAddedEvent(Lsa lsa, String bondId, Lsa bondedLsa){
		this.lsa = lsa;
		this.bondId = bondId;
		this.bondedLsa = bondedLsa;
	}
	
	public ILsa getLsa(){
		return lsa;
	}
	
	public String getBondId(){
		return bondId;
	}

	public ILsa getBondedLsa() {
		return bondedLsa;
	}

	public void setBondedLsa(Lsa bondedLsa) {
		this.bondedLsa = bondedLsa;
	}
	


}
