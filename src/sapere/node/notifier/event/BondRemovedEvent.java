/**
 * 
 */
package sapere.node.notifier.event;

import sapere.lsa.Lsa;
import sapere.lsa.interfaces.ILsa;


/**
 * @author Gabriella Castelli (UNIMORE)
 * @author Francesco De Angelis (UNIGE)
 *
 */
public class BondRemovedEvent extends AbstractSapereEvent{
	
	private Lsa lsa = null;
	private String bondId = null; // the the Id of the LSa to which I have just been bonded
	private Lsa removedLsa = null;
	
	public BondRemovedEvent(Lsa lsa, String bondId){
		this.lsa = lsa;
		this.bondId = bondId;
	}
	
	public BondRemovedEvent(Lsa lsa, String bondId, Lsa removedLsa){
		this.lsa = lsa;
		this.bondId = bondId;
		if (removedLsa != null)
			this.removedLsa = removedLsa.getCopy();
	}
	
	public Lsa getCopyRemovedLsa()
	{
		return removedLsa;
	}
	
	public ILsa getLsa(){
		return lsa;
	}
	
	public String getBondId(){
		return bondId;
	}


}