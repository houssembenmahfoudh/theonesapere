package sapere.node.networking;

import sapere.lsa.Lsa;



public interface INetworkReceiverManager  {

	public void doInject(Lsa receivedLsa);
	public void onLsaReceived(Lsa lsaReceived);

}
