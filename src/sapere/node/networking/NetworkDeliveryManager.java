package sapere.node.networking;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.UUID;
import java.util.Vector;
import java.util.logging.Logger;
import one.core.Connection;
import one.core.DTNHost;
import one.core.Message;
import one.core.SimClock;
import sapere.lsa.Lsa;
import sapere.lsa.Property;
import sapere.lsa.values.PropertyName;
import sapere.lsa.values.SyntheticPropertyName;
import com.google.common.base.Optional;
import extension.AbstractSapereHost;
import extension.SapereProperties;

public class NetworkDeliveryManager implements INetworkDeliveryManager {

	/**
	 * The class logger.
	 */
	private static final Logger LOGGER = Logger.getLogger("sapere.node.networking.NetworkDeliveryManager");

	/**
	 * The modes of transport.
	 */
	private enum Mode {
		SPREAD, DIRECT
	};

	/**
	 * The local host.
	 */
	final DTNHost my_host;

	private HashMap<String, Integer> gradientValue = new HashMap<String, Integer>();

	private HashMap<String, Vector<String>> gradientReceiver = new HashMap<String, Vector<String>>();

	/**
	 * Creates the delivery manager for a given host.
	 * 
	 * @param a_host
	 */
	public NetworkDeliveryManager(final DTNHost a_host) {
		my_host = a_host;
	}

	public boolean checkHopCount(Lsa an_lsa) {
		Property hopCountProp = an_lsa.getProperty(PropertyName.GRADIENT_HOP.toString());
		Property maxHopProp = an_lsa.getProperty(PropertyName.GRADIENT_MAX_HOP.toString());

		int hopCount = Integer.parseInt(hopCountProp.getValue().firstElement());
		int maxHop = Integer.parseInt(maxHopProp.getValue().firstElement());

		if (hopCount < maxHop)
			return true;
		else
			return false;
	}

	/**
	 * Creates a copy of an LSA to be propagated, removing the id, bonds and
	 * other properties.
	 * 
	 * @param an_lsa
	 *            the LSA to be copied.
	 * @param the_finalHop
	 *            true if this is the final hop for the LSA, false otherwise.
	 * @return the LSA copy.
	 */
	private Lsa gradientCopy(final Lsa an_lsa) {

		Lsa copy = an_lsa.getCopy();
		copy.removeSyntheticProperty(SyntheticPropertyName.BONDS);
		copy.removeSyntheticProperty(SyntheticPropertyName.CREATION_TIME);
		copy.removeSyntheticProperty(SyntheticPropertyName.CREATOR_ID);
		copy.removeSyntheticProperty(SyntheticPropertyName.LAST_MODIFIED);
		copy.removeSyntheticProperty(SyntheticPropertyName.LOCATION);
		copy.setId(null);
		int hop = Integer.parseInt(copy.getProperty(PropertyName.GRADIENT_HOP.toString()).getValue().firstElement());
		hop++;
		// set the new hop counter
		copy.getProperty(PropertyName.GRADIENT_HOP.toString()).getValue().set(0, "" + hop);
		String nodeName = this.my_host.getName();
		// set previous = this node
		copy.getProperty(PropertyName.GRADIENT_PREVIOUS.toString()).getValue().set(0, nodeName);

		return copy;
	}

	public boolean alreadySentGradient(String gid, DTNHost receiver) {

		if (gradientReceiver.get(gid) == null) {
			Vector<String> vc = new Vector<String>();
			vc.add(receiver.getName());

			gradientReceiver.put(gid, vc);
			return false;
		}

		Vector<String> receivers = gradientReceiver.get(gid);

		for (String nodeName : receivers) {
			if (nodeName.equals(receiver.getName())) {
				return true;
			}
		}
		receivers.add(receiver.getName());
		return false;
	}

	public boolean discardLsa(Lsa lsa) {
		boolean discard=false;
		String gId = lsa.getProperty(PropertyName.GRADIENT.toString()).getValue().firstElement();

		String sHop = lsa.getProperty(PropertyName.GRADIENT_HOP.toString()).getValue().firstElement();
		String sMaxHop = lsa.getProperty(PropertyName.GRADIENT_MAX_HOP.toString()).getValue().firstElement();

		int hopCounter = Integer.parseInt(sHop);
		int maxHop = Integer.parseInt(sMaxHop);

		// we had already incremented it in the copy
		if ((hopCounter - 1) > maxHop)
			return true;

		// check if an lsa for the same gradient with a less hop counter
		// has been already sent
		if (gradientValue.get(gId) == null) {
			gradientValue.put(gId, hopCounter);
			discard= true;
		} else {
			int vOld = gradientValue.get(gId);

			if (vOld <= hopCounter) {
				discard= true;
			} else if (vOld > hopCounter) {
				gradientValue.put(gId, hopCounter);
				discard= false;
			}
		}
		return discard;
	}

	/**
	 * {@inheritDoc}
	 */
	public boolean doSpread(final Lsa an_lsa) {

		LOGGER.finest("Spreading an LSA from " + my_host.getName());

		if (SapereProperties.BROADCAST) {
			SapereProperties.MESSAGE_COUNTER++;
		}

		// check hop counter
		if (checkHopCount(an_lsa) == false)
			return false;

		Property prevProp = an_lsa.getProperty(PropertyName.GRADIENT_PREVIOUS.toString());
		String previous = prevProp.getValue().firstElement();
		// create a new copy of the lsa
		Lsa lsa = gradientCopy(an_lsa);

		for (Connection connection : my_host.getConnections()) {
			final DTNHost neighbour = connection.getOtherNode(my_host);
			// check previous property
			if (previous.equals(neighbour.getName()))
				continue;

			String gid = lsa.getProperty(PropertyName.GRADIENT.toString()).getValue().firstElement();
			if (discardLsa(an_lsa) == true) {

				if (alreadySentGradient(gid, neighbour) == true){
					continue;
				}
			
			}

			 System.out.println(this.my_host.getName() + " sends to " + neighbour.getName());
			send(neighbour, lsa, Mode.SPREAD);
		}
		return true;
	}

	// added by Francesco De Angelis
	//
	public boolean isOncePropagation(Lsa lsa) {

		if (lsa.getProperty(PropertyName.DIFFUSION_ONCE.toString()) != null
				&& lsa.getProperty(PropertyName.DIFFUSION_ONCE.toString()).getValue().firstElement().equals("yes"))
			return true;
		else
			return false;
	}

	// added by Francesco De Angelis
	//
	public boolean isPropagationDone(Lsa lsa, DTNHost host) {

		String lsaId = lsa.getProperty("uid").getValue().firstElement();

		if (host.sentPropagationOnce.containsKey(lsaId) == false) {
			ArrayList<String> hosts = new ArrayList<String>();
			hosts.add("" + host.getAddress());
			host.sentPropagationOnce.put(lsaId, hosts);
			return false;
		} else {
			ArrayList<String> hosts = host.sentPropagationOnce.get(lsaId);

			if (hosts.contains("" + host.getAddress())) {
				return true;
			} else {
				hosts.add("" + host.getAddress());
				return false;
			}
		}
	}

	// modified by Francesco De Angelis
	//
	/**
	 * {@inheritDoc}
	 */
	public boolean sendDirect(String a_destinationName, Lsa an_lsa) {

		boolean successful = false;
		Optional<DTNHost> destinationHost = Optional.absent();

		if (my_host.getConnections() == null)
			return false;
		else if (my_host.getConnections() != null && my_host.getConnections().size() == 0)
			return false;
		for (Connection connection : my_host.getConnections()) {
			final DTNHost neighbour = connection.getOtherNode(my_host);
			if (neighbour.getName().equals(a_destinationName)) {
				destinationHost = Optional.fromNullable(neighbour);
				break;
			} else if (a_destinationName.equals("all")) {
				destinationHost = Optional.fromNullable(neighbour);

				if (isOncePropagation(an_lsa)) {
					if (isPropagationDone(an_lsa, destinationHost.get()) == false) {
						send(destinationHost.get(), an_lsa, Mode.DIRECT);
						successful = true;
					} else
						successful = false;
				} else {
					send(destinationHost.get(), an_lsa, Mode.DIRECT);
					successful = true;
				}
			}
		}

		if (a_destinationName.equals("all"))
			return successful;
		// If null, node is not a neighbour. Otherwise, send the message.
		if (destinationHost.isPresent()) {
			if (isOncePropagation(an_lsa)) {
				if (isPropagationDone(an_lsa, destinationHost.get()) == false) {
					send(destinationHost.get(), an_lsa, Mode.DIRECT);
					successful = true;
				} else
					successful = false;
			} else {
				send(destinationHost.get(), an_lsa, Mode.DIRECT);
				successful = true;
			}
			LOGGER.finest("Sent an LSA from " + my_host.getName() + " to " + a_destinationName);
		} else {
			LOGGER.warning("Failed to send an LSA from " + my_host.getName() + " to " + a_destinationName
					+ "(Nodes are not neighbours)");
		}
		return successful;
	}

	/**
	 * Sends an LSA direct to a destination.
	 * 
	 * @param a_destination
	 *            the destination node.
	 * @param an_lsa
	 *            the LSA to send.
	 * @param the
	 *            mode that triggered the message.
	 */
	private void send(final DTNHost a_destination, final Lsa an_lsa, final Mode a_mode) {

		// System.out.println(this.my_host.getName() + " SENDING AMESSAGE to " +
		// a_destination.getName());
		final String msgID = generateMessageId(a_destination, a_mode);
		final Message msg = new Message(my_host, a_destination, msgID, 0);
		msg.addProperty("lsa", an_lsa);
		msg.setAppID(AbstractSapereHost.APP_ID);
		my_host.createNewMessage(msg);
	}

	/**
	 * Generates a unique message ID for a message with a given destination.
	 * 
	 * @param a_destination
	 *            the destination node.
	 * @param a_mode
	 *            the mode of propagation.
	 * @return a unique message id.
	 */
	public String generateMessageId(final DTNHost a_destination, final Mode a_mode) {

		return a_mode.name() + ":" + my_host.getName() + "-" + a_destination.getName() + ":" + SimClock.getTime() + "-"
				+ UUID.randomUUID();
	}

}
