/**
 * 
 */
package sapere.node.util;

import java.io.FileInputStream;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * TO DO: Add the management of a configuration file
 * 
 * @author Gabriella Castelli (UNIMORE)
 * 
 */
public class SystemConfiguration {

	// ----------- Logging -----------------//
	/** Logging scope to use */
	private transient Logger log = Logger.getLogger("eu.sapere"); // Default
																	// application
																	// logger
	
	static String GLOBAL_PROP_FILE_NAME = "./conf/settings.properties";
	public static Properties props = null;

	private SystemConfiguration() {

		setLoggingLevel(Level.SEVERE);
		
		//---------- Read properties ------------//
  		props = new Properties();
  		try {
  			props.load(new FileInputStream(GLOBAL_PROP_FILE_NAME));
  		} catch (Exception e) {
  				System.err.println("Error while reading property files: " + GLOBAL_PROP_FILE_NAME);
  		}

	}

	/**
	 * Stores current configuration.
	 */
	private static SystemConfiguration instance;

	/**
	 * Retrieve current system configuration.
	 * 
	 * @return The configuration.
	 */
	public final static SystemConfiguration getInstance() {
		if (instance == null)
			instance = new SystemConfiguration();
		return instance;
	}

	/**
	 * @return the log
	 */
	public final Logger getLog() {
		return log;
	}

	/**
	 * Retrieve global logging level.
	 * 
	 * @return the loggingLevel
	 */
	public Level getLoggingLevel() {
		// to do
		return Level.INFO;
	}

	/**
	 * Store global logging level.
	 * 
	 * @param level
	 *            the loggingLevel to set
	 */
	public void setLoggingLevel(Level level) {
		Logger.getLogger("sapere").setLevel(level);

	}

}
