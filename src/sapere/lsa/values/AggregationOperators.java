package sapere.lsa.values;

import java.util.Date;
import java.util.Vector;

import sapere.lsa.Id;
import sapere.lsa.Lsa;
import sapere.lsa.Property;
import sapere.lsa.SyntheticProperty;
import sapere.lsa.exception.UnresolvedPropertyNameException;



public enum AggregationOperators {
		
		MAX ("max"),
		
		MIN ("min"), 
		
		AVG ("avg");
		
		private String text;

		AggregationOperators (String text) {
		    this.text = text;
		  }

		  public String getText() {
		    return this.text;
		  }
		
		public static Id max (Vector<Lsa> allLsa){
			
			Id ret = null;
			
			if(allLsa.isEmpty())
				return null;
			
			int max = new Integer(allLsa.elementAt(0).getProperty(allLsa.elementAt(0).getFieldValue()).getValue().elementAt(0)).intValue();
			ret = allLsa.elementAt(0).getId();
			
			for(int i=0; i<allLsa.size(); i++){
				if (new Integer(allLsa.elementAt(i).getProperty(allLsa.elementAt(0).getFieldValue()).getValue().elementAt(0)).intValue() > max){
					max = new Integer(allLsa.elementAt(i).getProperty(allLsa.elementAt(0).getFieldValue()).getValue().elementAt(0)).intValue();
					ret =  allLsa.elementAt(i).getId();
				}
			}
			
			return ret;
			
		}
		
		public static Id min (Vector<Lsa> allLsa){
			
			Id ret = null;
			
			if(allLsa.isEmpty())
				return null;
			
			int min = new Integer(allLsa.elementAt(0).getProperty(allLsa.elementAt(0).getFieldValue()).getValue().elementAt(0)).intValue();
			ret = allLsa.elementAt(0).getId();
			
			for(int i=0; i<allLsa.size(); i++){
				if (new Integer(allLsa.elementAt(i).getProperty(allLsa.elementAt(0).getFieldValue()).getValue().elementAt(0)).intValue() < min){
					min = new Integer(allLsa.elementAt(i).getProperty(allLsa.elementAt(0).getFieldValue()).getValue().elementAt(0)).intValue();
					ret =  allLsa.elementAt(i).getId();
				}
			}
			
			return ret;
			
		}
		
		public static Lsa mean (Vector<Lsa> allLsa){
			
			Lsa ret = null;
			
			if(allLsa.isEmpty())
				return null;
			
			int val = 0;
			
			for(int i=0; i<allLsa.size(); i++){
					val += new Integer(allLsa.elementAt(i).getProperty(allLsa.elementAt(0).getFieldValue()).getValue().elementAt(0)).intValue();
			}
			
			
			val = val / allLsa.size();
			
			ret = allLsa.elementAt(0).getCopy();
			try {
				ret.setProperty(new Property(allLsa.elementAt(0).getFieldValue(), ""+val));
			} catch (UnresolvedPropertyNameException e) {
				e.printStackTrace();
			}
			
				ret.addProperty(new SyntheticProperty(SyntheticPropertyName.LAST_MODIFIED, new String(""+new Date().getTime())));
			
			return ret;
			
		}

}
