package sapere.lsa.autoupdate;

public interface PropertyValueListener{
	
    public void PropertyValueGenerated( PropertyValueEvent event );
    
    public void PropertyValueAppendGenerated( PropertyValueEvent event );
    
}

