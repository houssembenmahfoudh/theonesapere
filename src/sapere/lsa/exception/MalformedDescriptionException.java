/**
 * 
 */
package sapere.lsa.exception;

/**
 * @author Gabriella Castelli (UNIMORE)
 *
 */
public class MalformedDescriptionException  extends RuntimeException {}
