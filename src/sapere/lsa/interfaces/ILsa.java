package sapere.lsa.interfaces;

import java.util.Set;

import com.google.common.base.Optional;

import sapere.lsa.Lsa;
import sapere.lsa.Property;
import sapere.lsa.SubDescription;
import sapere.lsa.exception.MalformedDescriptionException;
import sapere.lsa.exception.UnresolvedPropertyNameException;

/**
 * @author Gabriella Castelli (UNIMORE)
 */
public interface ILsa {

   public Lsa addProperty(Property p) throws MalformedDescriptionException;

   public Lsa addProperty(Property... properties);

   public Lsa removeProperty(String propertyName);

   public Lsa setProperty(Property p) throws UnresolvedPropertyNameException;

   public Property getProperty(String name);

   /**
    * Provides quick access to the first value of a property in the vector.
    * @return an Optional containing the first value in the vector if it exists.
    */
   public Optional<String> getSingleValue(String a_name);

   public SubDescription addSubDescription(String name, Property value);

   public SubDescription addSubDescription(String name, Property... values);

   public Lsa removeSubDescription(String name);

   public ISubDescription setSubDescription(String name, Property value);

   public SubDescription getSubDescriptionByName(String name);

   public Set<String> getSubDescriptionNames();

   public ILsa getCopy();
   
   public String toString();

}
