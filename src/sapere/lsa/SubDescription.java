/**
 * 
 */
package sapere.lsa;

import java.util.Map;

import com.google.common.base.Optional;

import sapere.lsa.values.SyntheticPropertyName;

/**
 * Represents a SubDescription
 * @author Gabriella Castelli (UNIMORE)
 */
public class SubDescription extends Description {

   public SubDescription(Id id, Property p) {
      super(id, p);
      // TODO Auto-generated constructor stub
   }

   public SubDescription(Id id, Property[] properties) {
      super(id, properties);
      // TODO Auto-generated constructor stub
   }

   protected SubDescription(Id id, Map<String, Property> Properties,
         Map<SyntheticPropertyName, Property> SyntheticProperties) {
      super(id, Properties, SyntheticProperties);
      // TODO Auto-generated constructor stub
   }

   private static final long serialVersionUID = 7319728270982187084L;

   @Override
   public SubDescription getCopy() {
      Description d = super.getCopy();
      SubDescription sd = new SubDescription(d.getId().getCopy(), d.Properties, d.SyntheticProperties);
      return sd;
   }

   /**
    * Returns the specified Property
    */
   public Property getProperty(String name) {
      return Properties.get(name);
   }

   /**
    * Provides quick access to the first value of a property in the vector.
    * @return an Optional containing the first value in the vector if it exists.
    */
   public Optional<String> getSingleValue(String name) {
      final Property property = Properties.get(name);
      if (property != null && property.getValue() != null && !property.getValue().isEmpty()) {
         return Optional.fromNullable(property.getValue().firstElement());
      } else {
         return Optional.absent();
      }
   }

   /**
    * Checks a SubDescription for property containment.
    * @param name the name of the property.
    * @return true if the property exists, false otherwise.
    */
   public boolean hasProperty(String name) {
      return Properties.containsKey(name);
   }

}
