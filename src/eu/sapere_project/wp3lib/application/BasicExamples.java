package eu.sapere_project.wp3lib.application;

import java.util.LinkedList;
import java.util.List;

import one.core.Application;
import one.core.Settings;
import sapere.agent.SapereAgent;
import sapere.node.lsaspace.ecolaws.Bonding;
import sapere.node.lsaspace.ecolaws.Decay;
import sapere.node.lsaspace.ecolaws.IEcoLaw;
import sapere.node.lsaspace.ecolaws.Propagation;
import eu.sapere_project.wp3lib.extension.agent.ExampleAggregationAgent;
import eu.sapere_project.wp3lib.extension.agent.ExampleBondingAgent;
import eu.sapere_project.wp3lib.extension.agent.ExampleDecayAgent;
import eu.sapere_project.wp3lib.extension.agent.ExampleGradientAgent;
import eu.sapere_project.wp3lib.extension.ecolaw.Aggregation2;
import eu.sapere_project.wp3lib.extension.ecolaw.AggregationPredicate.OP;
import extension.AbstractSapereHost;

/**
 * A suite of simple examples demonstrating eco-laws. Do not modify.
 */
public final class BasicExamples extends AbstractSapereHost {

	/**
	 * Creates a new BasicSapereHost.
	 */
	public BasicExamples(final Settings some_settings) {
		super(some_settings);
	}

	/**
	 * An example of a gradient.
	 */
	public void setupNode0() {
		// Node 0: 10 hop gradient
		SapereAgent agent = new ExampleGradientAgent("GradientAgent", getNodeManager());
		agent.setInitialLSA();
	}

	/**
	 * An example of decay.
	 */
	public void setupNode1() {
		SapereAgent agent = new ExampleDecayAgent("Decay Example", getNodeManager());
		agent.setInitialLSA();
	}

	/**
	 * Aggregation based on average.
	 */
	public void setupNode2() {
		SapereAgent agentA = new ExampleAggregationAgent(getHost().getName(), getNodeManager(), "5", OP.AVG);
		SapereAgent agentB = new ExampleAggregationAgent(getHost().getName(), getNodeManager(), "10", OP.AVG);
		agentA.setInitialLSA();
		agentB.setInitialLSA();
	}

	/**
	 * A simple bonding example.
	 */
	public void setupNode3() {
		SapereAgent agentA = new ExampleBondingAgent("BondingExample", getNodeManager(), "?");
		SapereAgent agentB = new ExampleBondingAgent("BondingExample", getNodeManager(), "some-value");
		agentA.setInitialLSA();
		agentB.setInitialLSA();
	}

	/**
	 * Sets up the eco-laws to be run in each node.
	 */
	public void setupAll() {
		final List<IEcoLaw> ecoLaws = new LinkedList<IEcoLaw>();
		ecoLaws.add(new Decay(getNodeManager()));
		ecoLaws.add(new Aggregation2(getNodeManager()));
		ecoLaws.add(new Bonding(getNodeManager()));
		ecoLaws.add(new Propagation(getNodeManager()));
		// ecoLaws.add(new Chemotaxis3(getNodeManager()));
		// getNodeManager().getEcoLawsEngine().replaceEcoLaws(ecoLaws);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void onSimulationCycle() {
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Application replicate() {
		return new BasicExamples(getSettings());
	}
}
