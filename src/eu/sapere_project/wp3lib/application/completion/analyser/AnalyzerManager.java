package eu.sapere_project.wp3lib.application.completion.analyser;

import sapere.node.NodeManager;

public class AnalyzerManager {

   private String name = null;

   private AnalyzerImplementation i = null;

   private AnalyzerService s = null;

   public AnalyzerManager(String name, NodeManager an_opMng) {

      this.name = name;
      this.startManager(an_opMng);
   }

   private void startManager(NodeManager an_opMng) {

      s = new AnalyzerService(name, an_opMng);
      i = new AnalyzerImplementation(name, s, an_opMng);

      s.setInitialLSA(i);
   }

}
