package eu.sapere_project.wp3lib.application.completion.adaptor;

import java.util.Vector;

import sapere.agent.SapereAgent;
import sapere.lsa.Property;
import sapere.node.NodeManager;
import sapere.node.notifier.event.BondAddedEvent;
import sapere.node.notifier.event.BondRemovedEvent;
import sapere.node.notifier.event.BondedLsaUpdateEvent;
import sapere.node.notifier.event.DecayedEvent;
import sapere.node.notifier.event.PropagationEvent;
import sapere.node.notifier.event.ReadEvent;

public class AdaptorImplementation extends SapereAgent {

   AdaptorService s = null;

   public AdaptorImplementation(String name, AdaptorService s, NodeManager a_nodeManager) {
      super(name, a_nodeManager.getOperationManager(), a_nodeManager.getNotifier());
      this.s = s;
   }

   public void setInitialLSA(Vector<String> v) {
      addProperty(new Property("gps-traces-analyzer", "?"), new Property("gps-traces", v), new Property("NAME", "p0"));
      addGradient(1, "MIN", "gradient-value");
   }

   @Override
   public void onBondAddedNotification(BondAddedEvent event) {
      // TODO Auto-generated method stub
   }

   @Override
   public void onBondRemovedNotification(BondRemovedEvent event) {
      // TODO Auto-generated method stub

   }

   @Override
   public void onBondedLsaUpdateEventNotification(BondedLsaUpdateEvent event) {

      Vector<String> res = event.getLsa().getProperty("gps-traces-analyzer").getPropertyValue().getValue();

      setProperty(new Property("gps-traces-analyzer", res));

      s.setResponse(res);

   }

   @Override
   public void setInitialLSA() {
      // TODO Auto-generated method stub

   }

   @Override
   public void onPropagationEvent(PropagationEvent event) {
      // TODO Auto-generated method stub

   }

   /**
    * {@inheritDoc}
    */
   @Override
   public void onReadNotification(ReadEvent readEvent) {
      // TODO Auto-generated method stub
      
   }

   /**
    * {@inheritDoc}
    */
   @Override
   public void onDecayedNotification(DecayedEvent event) {
      // TODO Auto-generated method stub
      
   }

}
