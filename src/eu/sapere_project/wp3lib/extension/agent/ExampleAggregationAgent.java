// [Project Title]
// (c) Copyright 2011, 2012 Graeme Stevenson (graeme.stevenson@st-andrews.ac.uk)
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions
// are met:
// 1. Redistributions of source code must retain the above copyright
// notice, this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright
// notice, this list of conditions and the following disclaimer in the
// documentation and/or other materials provided with the distribution.
// 3. The name of the author may not be used to endorse or promote products
// derived from this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
// IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
// OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
// IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
// INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
// NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
// DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
// THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
// THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
package eu.sapere_project.wp3lib.extension.agent;

import java.util.logging.Logger;

import sapere.agent.SapereAgent;
import sapere.lsa.Property;
import sapere.node.NodeManager;
import sapere.node.notifier.event.BondAddedEvent;
import sapere.node.notifier.event.BondRemovedEvent;
import sapere.node.notifier.event.BondedLsaUpdateEvent;
import sapere.node.notifier.event.DecayedEvent;
import sapere.node.notifier.event.PropagationEvent;
import sapere.node.notifier.event.ReadEvent;
import eu.sapere_project.wp3lib.extension.ecolaw.AggregationPredicate.OP;
import eu.sapere_project.wp3lib.extension.templates.AggregationInvoker;

/**
 * @author Graeme Stevenson (graeme.stevenson@st-andrews.ac.uk)
 */
public class ExampleAggregationAgent extends SapereAgent {

   private static final Logger LOGGER = Logger.getLogger("extension.agent.example");

   private final String my_nodeName;

   private final String my_value;

   private final OP my_operation;

   /**
    * Creates a new agent.
    * @param name the name of the node
    * @param an_opMng the operation manager for the node.
    */
   public ExampleAggregationAgent(String name, NodeManager a_nodeManager, String a_value, OP an_operation) {
      super(name, a_nodeManager.getOperationManager(), a_nodeManager.getNotifier());
      my_nodeName = name;
      my_value = a_value;
      my_operation = an_operation;
   }

   /**
    * {@inheritDoc}
    */
   public void onBondAddedNotification(BondAddedEvent event) {
      LOGGER.info("BONDED");
   }

   /**
    * {@inheritDoc}
    */
   public void onBondRemovedNotification(BondRemovedEvent event) {
      LOGGER.info("BOND REMOVED");
   }

   /**
    * {@inheritDoc}
    */
   public void onBondedLsaUpdateEventNotification(BondedLsaUpdateEvent event) {
      LOGGER.info("BOND UPDATED");
   }

   /**
    * {@inheritDoc}
    */
   public void onPropagationEvent(PropagationEvent event) {
      LOGGER.info("PROPAGATION EVENT");
   }

   /**
    * {@inheritDoc}
    */
   @Override
   public void setInitialLSA() {
      lsa.addProperty(new Property("NAME", my_nodeName));
      lsa.addProperty(new Property("Example", "average-example"));
      lsa.addProperty(new Property("aggregationValue", my_value));
      new AggregationInvoker("source", "AggregationApplication", "aggregationValue", my_operation).applyToLSA(lsa);
      submitOperation();
   }

   public void onReadNotification(ReadEvent readEvent) {
      // TODO Auto-generated method stub

   }

   /**
    * {@inheritDoc}
    */
   @Override
   public void onDecayedNotification(DecayedEvent event) {
      // TODO Auto-generated method stub

   }
}
