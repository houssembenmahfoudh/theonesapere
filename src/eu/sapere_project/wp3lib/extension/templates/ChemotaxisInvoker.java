// Taken from the SAPERE WP3 Libraries
// (c) Copyright 2011, 2012 Graeme Stevenson (graeme.stevenson@st-andrews.ac.uk)
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions
// are met:
// 1. Redistributions of source code must retain the above copyright
// notice, this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright
// notice, this list of conditions and the following disclaimer in the
// documentation and/or other materials provided with the distribution.
// 3. The name of the author may not be used to endorse or promote products
// derived from this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
// IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
// OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
// IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
// INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
// NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
// DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
// THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
// THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
package eu.sapere_project.wp3lib.extension.templates;

import sapere.lsa.Property;
import sapere.lsa.interfaces.ILsa;
import sapere.lsa.values.AggregationOperators;
import sapere.lsa.values.PropertyName;

/**
 * Applies chemotaxis to an LSA.
 * @author Graeme Stevenson (graeme.stevenson@st-andrews.ac.uk)
 */
public final class ChemotaxisInvoker implements Pattern {

   /**
    * The options for the chemotaxis mode.
    */
   public enum Mode {
      SINGLE, WITH_AGGREGATION, PERSIST
   };

   /**
    * The response source.
    */
   private final String my_sourceName;

   /**
    * The source name attached to the gradient to follow.
    */
   private final String my_destinationName;

   /**
    * The response Id.
    */
   private final String my_responseId;

   /**
    * The source name attached to the gradient to follow.
    */
   private final Mode my_mode;

   /**
    * The aggregation value field.
    */
   private final String my_aggregationField;

   /**
    * The aggregation operator to apply to two LSAs from a single source.
    */
   private final AggregationOperators my_aggregationOperator;

   /**
    * Creates a chemotaxis template following a gradient to a given source.
    * @param a_destinationName the source name for this gradient
    * @param a_sourceName the name of the chemotaxis source.
    * @param a_responseId the response identifier.
    */
   public ChemotaxisInvoker(final String a_sourceName, final String a_destinationName, final String a_responseId) {
      my_sourceName = a_sourceName;
      my_destinationName = a_destinationName;
      my_aggregationField = null;
      my_aggregationOperator = null;
      my_mode = Mode.SINGLE;
      my_responseId = a_responseId;
   }

   /**
    * Creates a chemotaxis template following a gradient to a given source and applying aggregation
    * to multiple replies from the same source co-located along the route.
    * @param a_destinationName the source name for this gradient
    * @param a_sourceName the name of the chemotaxis source.
    * @param an_aggregationField the field to aggregate on.
    * @param an_aggregationOperator the aggregation operator for the field.
    */
   public ChemotaxisInvoker(final String a_sourceName, final String a_destinationName,
         final String an_aggregationField, final String a_responseId, final AggregationOperators an_aggregationOperator) {
      my_sourceName = a_sourceName;
      my_destinationName = a_destinationName;
      my_aggregationField = an_aggregationField;
      my_responseId = a_responseId;
      my_aggregationOperator = an_aggregationOperator;
      my_mode = Mode.WITH_AGGREGATION;
   }

   /**
    * {@inheritDoc}
    */
   @Override
   public void applyToLSA(final ILsa an_lsa) {
      an_lsa.addProperty(new Property("chemotaxisDestination", my_destinationName));
      an_lsa.addProperty(new Property("chemotaxisMode", my_mode.toString().toLowerCase()));
      an_lsa.addProperty(new Property("chemotaxisResponseId", my_responseId));
      // If aggregation is to be applied.
      if (my_mode.equals(Mode.WITH_AGGREGATION)) {
         an_lsa.addProperty(new Property(PropertyName.AGGREGATION_OP.toString(), my_aggregationOperator.name()));
         an_lsa.addProperty(new Property(PropertyName.FIELD_VALUE.toString(), my_aggregationField));
         an_lsa.addProperty(new Property(PropertyName.SOURCE.toString(), my_sourceName));
      }

      if (my_mode.equals(Mode.PERSIST)) {
         System.err.println("PERSISTENCE MODE NOT SET IN CHEMOTAXIS");
         // an_lsa.addProperty(new Property(PropertyName.AGGREGATION_OP.toString(),
         // AggregationOperators.MAX.toString()));
         // an_lsa.addProperty(new Property(PropertyName.FIELD_VALUE.toString(), "lastUpdatedAt"));
         // an_lsa.addProperty(new Property(PropertyName.SOURCE.toString(), my_sourceName));
      }
   }

   /**
    * {@inheritDoc}
    */
   @Override
   public void removeFromLSA(final ILsa an_lsa) {
      an_lsa.removeProperty("chemotaxisDestination");
      an_lsa.removeProperty("chemotaxisMode");
      an_lsa.removeProperty("chemotaxisResponseId");
      an_lsa.removeProperty(PropertyName.SOURCE.toString());
      an_lsa.removeProperty(PropertyName.AGGREGATION_OP.toString());
      an_lsa.removeProperty(PropertyName.FIELD_VALUE.toString());
   }

}
