// Taken from the SAPERE WP3 Libraries
// (c) Copyright 2011, 2012 Graeme Stevenson (graeme.stevenson@st-andrews.ac.uk)
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions
// are met:
// 1. Redistributions of source code must retain the above copyright
// notice, this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright
// notice, this list of conditions and the following disclaimer in the
// documentation and/or other materials provided with the distribution.
// 3. The name of the author may not be used to endorse or promote products
// derived from this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
// IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
// OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
// IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
// INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
// NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
// DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
// THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
// THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
package eu.sapere_project.wp3lib.extension.templates;

import sapere.lsa.Property;
import sapere.lsa.interfaces.ILsa;

/**
 * Applies chemotaxis to an LSA.
 * @author Graeme Stevenson (graeme.stevenson@st-andrews.ac.uk)
 */
public final class AdvertiseInvoker implements Pattern {

   /**
    * The source name for the advertisement.
    */
   private final String my_sourceName;

   /**
    * Creates an advertisement invoker for a source.
    * @param an_advertisementName the name to advertise this agent.
    */
   public AdvertiseInvoker(final String an_advertisementName) {
      my_sourceName = an_advertisementName;
   }

   /**
    * {@inheritDoc}
    */
   @Override
   public void applyToLSA(final ILsa an_lsa) {
      an_lsa.addProperty(new Property("advertise_name", my_sourceName));
      an_lsa.addProperty(new Property("advertise", "true"));
   }

   /**
    * Utility method to apply to an LSA and return the template to support assignment.
    * @param an_advertisementKeyword the advertisement keyword.
    * @param an_lsa the LSA to apply the transform to.
    * @return the invoker, after application to the LSA.
    */
   public static AdvertiseInvoker createAndApply(final String an_advertisementKeyword, final ILsa an_lsa) {
      final AdvertiseInvoker result = new AdvertiseInvoker(an_advertisementKeyword);
      result.applyToLSA(an_lsa);
      return result;
   }

   /**
    * {@inheritDoc}
    */
   public void removeFromLSA(final ILsa an_lsa) {
      an_lsa.removeProperty("advertise_name");
      an_lsa.removeProperty("advertise");
   }

}
