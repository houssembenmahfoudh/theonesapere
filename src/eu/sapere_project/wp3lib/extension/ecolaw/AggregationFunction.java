// [Project Title]
// (c) Copyright 2011, 2012 Graeme Stevenson (graeme.stevenson@st-andrews.ac.uk)
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions
// are met:
// 1. Redistributions of source code must retain the above copyright
// notice, this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright
// notice, this list of conditions and the following disclaimer in the
// documentation and/or other materials provided with the distribution.
// 3. The name of the author may not be used to endorse or promote products
// derived from this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
// IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
// OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
// IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
// INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
// NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
// DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
// THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
// THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
package eu.sapere_project.wp3lib.extension.ecolaw;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import sapere.lsa.Lsa;
import sapere.lsa.Property;
import sapere.lsa.values.PropertyName;
import com.google.common.collect.Lists;
import eu.sapere_project.wp3.situation_framework.ensemble.aggregation.Aggregator;
import eu.sapere_project.wp3.situation_framework.ensemble.functions.DempsterShafer;
import eu.sapere_project.wp3.situation_framework.ensemble.util.Marshall;
import eu.sapere_project.wp3lib.extension.ecolaw.AggregationPredicate.OP;

/**
 * A class containing the functions for applying different types of aggregation
 * to LSAs.
 * 
 * @author Graeme Stevenson (graeme.stevenson@st-andrews.ac.uk)
 */
public final class AggregationFunction {
	/**
	 * Decimal format to 3 places.
	 */
	public static DecimalFormat THREE_D = new DecimalFormat("##.###");

	/**
	 * Aggregates a set of LSAs according to an operator.
	 * 
	 * @param an_operator
	 *            the aggregation operator to apply.
	 * @param some_lsas
	 *            the LSAs to aggregate. The list should have size > 2 (enforced
	 *            in calling method).
	 * @param a_field
	 *            the name of the field to aggregate.
	 * @return the aggregated LSA, whether new, or from the original list (new
	 *         LSAs have no Id).
	 */
	public static Lsa apply(final OP an_operator, final String a_field, final Collection<Lsa> some_lsas) {
		// Shortcut if there is only one LSA.
		if (some_lsas.size() == 1) {
			return some_lsas.iterator().next();
		}

		switch (an_operator) {
		case MIN:
			return min(a_field, some_lsas);
		case MAX:
			return max(a_field, some_lsas);
		case AVG:
			return avg(a_field, some_lsas);
		case DST:
			return dst(a_field, some_lsas);
		default:
			return null;
		}

	}

	/**
	 * An implementation of MIN aggregation.
	 * 
	 * @param a_field
	 *            the field containing the value to aggregate.
	 * @param some_lsas
	 *            the LSAs to aggregate.
	 * @return the aggregated value encapsulated in an LSA.
	 */
	private static Lsa min(final String a_field, final Collection<Lsa> some_lsas) {
		// The minimum value and the LSA that holds it.
		BigDecimal minimumValue = BigDecimal.valueOf(Double.MAX_VALUE);
		Lsa result = null;

		for (Lsa lsa : some_lsas) {
			// 1. Extract value to be aggregated. We know this exists from the
			// initial filter.
			final BigDecimal value = new BigDecimal(lsa.getSingleValue(a_field).get());
			// 2. Select based on the current value.
			if (value.compareTo(minimumValue) < 0) {
				result = lsa;
				minimumValue = value;
			}
		}
		return result;
	}

	/**
	 * An implementation of MAX aggregation.
	 * 
	 * @param a_field
	 *            the field containing the value to aggregate.
	 * @param some_lsas
	 *            the LSAs to aggregate.
	 * @return the aggregated value encapsulated in an LSA.
	 */
	private static Lsa max(final String a_field, final Collection<Lsa> some_lsas) {
		// The maximum value and the LSA that holds it.
		BigDecimal maximumValue = BigDecimal.valueOf(Double.MIN_VALUE);
		Lsa result = null;

		for (Lsa lsa : some_lsas) {
			// 1. Extract value to be aggregated. We know this exists from the
			// initial filter.
			final BigDecimal value = new BigDecimal(lsa.getSingleValue(a_field).get());
			// 2. Select based on the current value.
			if (value.compareTo(maximumValue) > 0) {
				result = lsa;
				maximumValue = value;
			}
		}
		return result;
	}

	/**
	 * An implementation of AVG aggregation.
	 * 
	 * @param a_field
	 *            the field containing the value to aggregate.
	 * @param some_lsas
	 *            the LSAs to aggregate.
	 * @return the aggregated value encapsulated in an LSA.
	 */
	private static Lsa avg(final String a_field, final Collection<Lsa> some_lsas) {
		// The maximum value and the LSA that holds it.
		BigDecimal totalValue = BigDecimal.valueOf(Double.MIN_VALUE);

		for (Lsa lsa : some_lsas) {
			// 1. Extract value to be aggregated. We know this exists from the
			// initial filter.
			final BigDecimal value = new BigDecimal(lsa.getSingleValue(a_field).get());
			// 2. Add value to the running total.
			totalValue = totalValue.add(value);
		}
		// 3. Calculate average.
		final BigDecimal average = totalValue.divide(BigDecimal.valueOf(some_lsas.size()), 3, RoundingMode.HALF_UP);
		// 4. Construct new LSA.
		final Lsa result = some_lsas.iterator().next().getCopy();
		result.setId(null);
		result.addProperty(new Property(a_field, average.toPlainString()));
		return result;
	}

	/**
	 * An implementation of DempsterShafer aggregation.
	 * 
	 * @param a_field
	 *            the field containing the value to aggregate.
	 * @param some_lsas
	 *            the LSAs to aggregate.
	 * @return the aggregated value encapsulated in an LSA.
	 */
	private static Lsa dst(final String a_field, final Collection<Lsa> some_lsas) {
		// The aggregator that will hold the results.
		final Aggregator<String> aggregator = new DempsterShafer<String>();
		// The new source name.
		final List<String> sourceList = Lists.newLinkedList();
		// The max decay value
		int maxDecay = 0;

		// Perform the aggregation
		for (final Lsa lsa : some_lsas) {
			// 1. Extract value to be aggregated. We know this exists from the
			// initial filter.
			final String valueToBeAggregated = lsa.getSingleValue(a_field).get();
			// 2. Marshall to aggregator.
			final Aggregator<String> convertedEntry = Marshall.bridgeStringToAggregator(valueToBeAggregated);

			// 3. Merge with current aggregate value.
			aggregator.aggregate(convertedEntry.getAggregate());
			// 4. Deconstruct, then add the source name to the aggregate source
			// list.
			sourceList.addAll(Arrays.asList(lsa.getSource().split(":")));
			// 5. Extract the decay value and compare to the current max.
			maxDecay = Math.max(maxDecay, Integer.parseInt(lsa.getSingleValue("decay").get()));
		}
		// 5. Calculate aggregate.
		final String aggregate = Marshall.bridgeAggregatorToString(aggregator);
		// 6. Construct new LSA.
		final Lsa result = some_lsas.iterator().next().getCopy();
		result.setId(null);
		Marshall.bridgeStringToAggregator(aggregate);
		result.addProperty(new Property(a_field, aggregate));

		// 7. Generate the new source name.
		final StringBuilder builder = new StringBuilder();
		Collections.sort(sourceList);

		for (final String name : sourceList) {
			builder.append(name + ":");
		}
		final String newSource = builder.toString();
		result.addProperty(
				new Property("most_probable_situation", aggregator.mostProbable("UNKNOWN").getKey().toString()));
		result.addProperty(new Property("most_probable_confidence",
				THREE_D.format(aggregator.mostProbable("UNKNOWN").getValue())));
		String newSourceName = newSource.substring(0, newSource.length() - 1);
		result.addProperty(new Property(PropertyName.SOURCE.toString(), newSourceName));
		result.addProperty(new Property("decay", String.valueOf(maxDecay)));

		// 8. Update the Max aggregation on the LSA.
		result.getSubDescriptionByName("agg:MAX")
				.setProperty(new Property(PropertyName.SOURCE.toString(), newSourceName));

		return result;
	}
}
