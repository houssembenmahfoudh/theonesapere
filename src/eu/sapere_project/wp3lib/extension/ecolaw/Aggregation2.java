package eu.sapere_project.wp3lib.extension.ecolaw;

import java.util.Collection;
import java.util.Map;

import sapere.lsa.Lsa;
import sapere.lsa.SubDescription;
import sapere.lsa.values.PropertyName;
import sapere.node.NodeManager;
import sapere.node.lsaspace.EcoLawsEngine;
import sapere.node.lsaspace.ecolaws.AbstractEcoLaw;

import com.google.common.collect.Collections2;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.rits.cloning.Cloner;

import eu.sapere_project.wp3lib.extension.ecolaw.AggregationPredicate.OP;
import eu.sapere_project.wp3lib.extension.ecolaw.filter.PropertyFilter;

/**
 * An implementation of an aggregation eco-law.
 * @author Graeme Stevenson (STA)
 */
public final class Aggregation2 extends AbstractEcoLaw {

   /**
    * Creates a new instance of the aggregation eco-law. Functionality covers MIN, MAX, AVE, and
    * DST.
    * @param a_nodeManager the node manager of the space.
    */
   public Aggregation2(final NodeManager a_nodeManager) {
      super(a_nodeManager);
   }

   /**
    * Invokes the different types of aggregation.
    */
   public void invoke() {
      // invokeAggregation(OP.MIN);
      invokeAggregation(OP.MAX);
      // invokeAggregation(OP.AVG);
      invokeAggregation(OP.DST);
   }

   /**
    * Iterates through all LSAs in a space, performing DST aggregation on those fitting the
    * criteria.
    * @param an_operator the aggregation operator.
    */
   public void invokeAggregation(final OP an_operator) {
      // 1. Collect all LSAs to be processed.
      final Collection<Lsa> aggregationLsas = Collections2.filter(getLSAs(), AggregationPredicate.create(an_operator));

      // 2. Create an index of all LSAs to be processed.
      final Map<String, Lsa> lsaIndex = createIndex(aggregationLsas);

      // 3. Iterate through all LSAs.
      for (final Lsa lsa : aggregationLsas) {

         // 4. If LSA is no longer in the index (i.e., priorly aggregated), skip the remainder.
         if (lsaIndex.containsKey(lsa.getId().toString())) {
            // 5. Find compatible LSAs from the source list.
            final SubDescription subDescription = lsa.getSubDescriptionByName("agg:" + an_operator.name());
            final Collection<Lsa> compatableLsas = getCompatableLSAs(lsa, subDescription, lsaIndex.values());

            // 6. Create/Select aggregate LSA.
            final String fieldName = subDescription.getSingleValue(PropertyName.FIELD_VALUE.toString()).get();
            final Lsa result = AggregationFunction.apply(an_operator, fieldName, compatableLsas);

            // 7. Remove aggregated LSAs from the LSA index (all) and space (excluding result).
            final boolean resultIsNewLsa = result.getId() == null;
            for (final Lsa compatible : compatableLsas) {
               lsaIndex.remove(compatible.getId().toString());
               if (resultIsNewLsa || !compatible.getId().toString().equals(result.getId().toString())) {
                  remove(compatible);
               }
            }

            // 8. If the result is a new LSA, inject it
            if (resultIsNewLsa) {
               inject(result);
            }
         }
      }
   }

   /**
    * Creates an index of a collection of LSAs by their Id.
    * @param some_lsas the collection of LSAs to index.
    * @return the index of LSAs.
    */
   public Map<String, Lsa> createIndex(final Collection<Lsa> some_lsas) {
      final Map<String, Lsa> lsaIndex = Maps.newHashMap();
      for (final Lsa lsa : some_lsas) {
         lsaIndex.put(lsa.getId().toString(), lsa.getCopy());
      }
      return lsaIndex;
   }

   /**
    * Collects LSAs to be aggregated compatible with an input.
    * @param an_lsa the template LSA.
    * @param a_sub the subDescription that matched the operator.
    * @param some_lsas the set of all candidate LSAs.
    * @return a deep clone of the collection of compatible LSAs for aggregation.
    */
   private Collection<Lsa> getCompatableLSAs(final Lsa an_lsa, final SubDescription a_sub,
         final Collection<Lsa> some_lsas) {
      Collection<Lsa> result;
      final OP operator = OP.valueOf(a_sub.getSingleValue(PropertyName.AGGREGATION_OP.toString()).get());
      if (OP.DST.equals(operator)) {
         result = Lists.newLinkedList();
         // Construct the predicate required to find LSAs that can be aggregated with this one.
         final AggregationPredicate predicate = getDSTAggregationPredicate(an_lsa, a_sub);
         // Create a collection based on the predicate.
         final Collection<Lsa> intermediateResult = Collections2.filter(some_lsas, predicate);
         // We can only apply this pairwise, so we filter the results to include only 1) this, 2)
         // one other LSA that this source has not been aggregated with.
         if (intermediateResult.size() > 0) {
            result.add(intermediateResult.iterator().next());
         }
         // Add the input LSA to the result collection.
         result.add(an_lsa);
      } else {
         // Construct the predicate required to match this LSA.
         final AggregationPredicate predicate = getGeneralAggregationPredicate(an_lsa, a_sub);

         // Create a collection based on the extracted fields.
         result = Collections2.filter(some_lsas, predicate);
      }

      // Return deep copy of the result to separate from iteration over the index.
      return new Cloner().deepClone(result);
   }

   /**
    * Constructs the aggregation predicate for the given aggregation description.
    * @param an_lsa the LSA with an aggregation description.
    * @param a_sub the subdescription containing the aggregation description.
    * @return the predicate fromed from the aggregation description.
    */
   public AggregationPredicate getGeneralAggregationPredicate(final Lsa an_lsa, final SubDescription a_sub) {
      final OP operator = OP.valueOf(a_sub.getSingleValue(PropertyName.AGGREGATION_OP.toString()).get());
      final String sourceName = an_lsa.getSource();
      final String aggregateField = a_sub.getSingleValue(PropertyName.FIELD_VALUE.toString()).get();
      return AggregationPredicate.create(operator).with(PropertyFilter.equals("source", sourceName))
            .with(PropertyFilter.exists(aggregateField));
   }

   /**
    * Constructs the aggregation predicate for the DST aggregation description.
    * @param an_lsa the LSA with an aggregation description.
    * @param a_sub the subdescription containing the aggregation description.
    * @return the predicate fromed from the aggregation description.
    */
   public AggregationPredicate getDSTAggregationPredicate(final Lsa an_lsa, final SubDescription a_sub) {
      final OP operator = OP.valueOf(a_sub.getSingleValue(PropertyName.AGGREGATION_OP.toString()).get());
      final String sourceName = an_lsa.getSource();
      final String responseId = a_sub.getSingleValue("chemotaxisResponseId").get();
      final String aggregateField = a_sub.getSingleValue(PropertyName.FIELD_VALUE.toString()).get();
      return AggregationPredicate.create(operator)
            .with(PropertyFilter.equals("chemotaxisResponseId", responseId))
            .with(PropertyFilter.exists(aggregateField)).with(PropertyFilter.doesNotContain("source", sourceName));
   }

   /**
    * {@inheritDoc}
    */
   @Override
   public boolean doesSpread() {
      return false;
   }

   /**
    * {@inheritDoc}
    */
   @Override
   public boolean doesDirectPropagation() {
      return false;
   }

   /**
    * {@inheritDoc}
    */
   @Override
   public String getName() {
      return EcoLawsEngine.ECO_LAWS_AGGREGATION;
   }

}
