package one.gui.playfield;

import java.awt.Color;
import java.util.HashMap;
import java.util.Random;

public class GradientColor {

	private HashMap<String,Color> hm;
	private Color color;
	
	public GradientColor()
	{
		hm = new HashMap<String,Color>();
	}
	
	public void addConnection(String gradientId)
	{
		if (hm.containsKey(gradientId))
			return;
		
		// generate a random color
		Random rn = new Random();
		int r = (Math.abs(rn.nextInt()) % 254) + 1;
		int g = (Math.abs(rn.nextInt()) % 254) + 1;
		int b = (Math.abs(rn.nextInt()) % 254) + 1;
		//System.out.println("Color: r " + r + " b " + b + " g " + g);
		hm.clear();
		hm.put(gradientId, new Color(r,g,b));
	}
	
	public void setNewColor()
	{
		// generate a random color
		Random rn = new Random();
		int r = (Math.abs(rn.nextInt()) % 254) + 1;
		int g = (Math.abs(rn.nextInt()) % 100) + 1;
		int b = (Math.abs(rn.nextInt()) % 254) + 1;
		color = new Color(r,g,b);
	}
	
	public Color getColor()
	{
		return color;
	}
	
	public Color getColorConnection(String gradientId)
	{
		return (Color)hm.get(gradientId);
	}
	
}
