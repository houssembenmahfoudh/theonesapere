/*
 * Copyright 2010 Aalto University, ComNet Released under GPLv3. See LICENSE.txt for details.
 */
package one.core;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Random;
import java.util.Set;
import one.input.EventQueue;
import one.input.ExternalEvent;
import one.input.ScheduledUpdatesQueue;
import one.interfaces.ConnectivityGrid;
import extension.AbstractSapereHost;
import extension.SapereProperties;

/**
 * World contains all the nodes and is responsible for updating their location
 * and connections.
 */
public class World {
	/** namespace of optimization settings ({@value} ) */
	public static final String SETTINGS_NS = "Optimization";

	private boolean convergence = false;

	/**
	 * Cell based optimization cell size multiplier -setting id ({@value} ).
	 * Single ConnectivityCell's size is the biggest radio range times this.
	 * Larger values save memory and decrease startup time but may result in
	 * slower simulation. Default value is {@link #DEF_CON_CELL_SIZE_MULT}.
	 * Smallest accepted value is 2.
	 * 
	 * @see ConnectivityGrid
	 */
	public static final String CELL_SIZE_MULT_S = "cellSizeMult";

	/**
	 * Should the order of node updates be different (random) within every
	 * update step -setting id ({@value} ). Boolean (true/false) variable.
	 * Default is @link {@link #DEF_RANDOMIZE_UPDATES}.
	 */
	public static final String RANDOMIZE_UPDATES_S = "randomizeUpdateOrder";

	/** default value for cell size multiplier ({@value} ) */
	public static final int DEF_CON_CELL_SIZE_MULT = 5;

	/**
	 * should the update order of nodes be randomized -setting's default value
	 * ({@value} )
	 */
	public static final boolean DEF_RANDOMIZE_UPDATES = true;

	private int sizeX;

	private int sizeY;

	private List<EventQueue> eventQueues;

	private double updateInterval;

	private SimClock simClock;

	private double nextQueueEventTime;

	private EventQueue nextEventQueue;

	/** list of nodes; nodes are indexed by their network address */
	private List<DTNHost> hosts;

	private boolean simulateConnections;

	/**
	 * nodes in the order they should be updated (if the order should be
	 * randomized; null value means that the order should not be randomized)
	 */
	private ArrayList<DTNHost> updateOrder;

	/** is cancellation of simulation requested from UI */
	private boolean isCancelled;

	private List<UpdateListener> updateListeners;

	/** Queue of scheduled update requests */
	private ScheduledUpdatesQueue scheduledUpdates;

	/** single ConnectivityCell's size is biggest radio range times this */
	private int conCellSizeMult;

	/** List of nodes to create the real gradient and evaluate the SAPERE one */
	private HashMap<String, Integer> nodeList = new HashMap<String, Integer>();

	private double evaluationAvg = 0; // to evaluate gradients
	private double evaluationCounter = 0; // to evaluate gradients

	/**
	 * Constructor.
	 */
	public World(List<DTNHost> hosts, int sizeX, int sizeY, double updateInterval, List<UpdateListener> updateListeners,
			boolean simulateConnections, List<EventQueue> eventQueues) {
		this.hosts = hosts;
		this.sizeX = sizeX;
		this.sizeY = sizeY;
		this.updateInterval = updateInterval;
		this.updateListeners = updateListeners;
		this.simulateConnections = simulateConnections;
		this.eventQueues = eventQueues;

		this.simClock = SimClock.getInstance();
		this.scheduledUpdates = new ScheduledUpdatesQueue();
		this.isCancelled = false;

		setNextEventQueue();
		initSettings();
	}

	/**
	 * Initializes settings fields that can be configured using Settings class
	 */
	private void initSettings() {
		Settings s = new Settings(SETTINGS_NS);
		boolean randomizeUpdates = DEF_RANDOMIZE_UPDATES;

		if (s.contains(RANDOMIZE_UPDATES_S)) {
			randomizeUpdates = s.getBoolean(RANDOMIZE_UPDATES_S);
		}
		if (randomizeUpdates) {
			// creates the update order array that can be shuffled
			this.updateOrder = new ArrayList<DTNHost>(this.hosts);
		} else { // null pointer means "don't randomize"
			this.updateOrder = null;
		}

		if (s.contains(CELL_SIZE_MULT_S)) {
			conCellSizeMult = s.getInt(CELL_SIZE_MULT_S);
		} else {
			conCellSizeMult = DEF_CON_CELL_SIZE_MULT;
		}

		// check that values are within limits
		if (conCellSizeMult < 2) {
			throw new SettingsError(
					"Too small value (" + conCellSizeMult + ") for " + SETTINGS_NS + "." + CELL_SIZE_MULT_S);
		}
	}

	/**
	 * Moves hosts in the world for the time given time initialize host
	 * positions properly. SimClock must be set to <CODE>-time</CODE> before
	 * calling this method.
	 * 
	 * @param time
	 *            The total time (seconds) to move
	 */
	public void warmupMovementModel(double time) {
		if (time <= 0) {
			return;
		}

		while (SimClock.getTime() < -updateInterval) {
			moveHosts(updateInterval);
			simClock.advance(updateInterval);
		}

		double finalStep = -SimClock.getTime();

		moveHosts(finalStep);
		simClock.setTime(0);
	}

	/**
	 * Goes through all event Queues and sets the event queue that has the next
	 * event.
	 */
	public void setNextEventQueue() {
		EventQueue nextQueue = scheduledUpdates;
		double earliest = nextQueue.nextEventsTime();

		/* find the queue that has the next event */
		for (EventQueue eq : eventQueues) {
			if (eq.nextEventsTime() < earliest) {
				nextQueue = eq;
				earliest = eq.nextEventsTime();
			}
		}

		this.nextEventQueue = nextQueue;
		this.nextQueueEventTime = earliest;
	}

	/**
	 * Update (move, connect, disconnect etc.) all hosts in the world. Runs all
	 * external events that are due between the time when this method is called
	 * and after one update interval.
	 */
	public void update() {
		double runUntil = SimClock.getTime() + this.updateInterval;

		// we increment the number of simulation cycles
		SapereProperties.CYCLE_COUNTER++;

		setNextEventQueue();

		/* process all events that are due until next interval update */
		while (this.nextQueueEventTime <= runUntil) {
			simClock.setTime(this.nextQueueEventTime);
			ExternalEvent ee = this.nextEventQueue.nextEvent();
			ee.processEvent(this);
			updateHosts(); // update all hosts after every event
			setNextEventQueue();
		}

		moveHosts(this.updateInterval);
		simClock.setTime(runUntil);

		updateHosts();

		// Should only be called once per tick
		updateSapereHosts();

		// System.out.println("maxAdj = " + maxAdj + " minAdj = " + minAdj +
		// " avg = " + avg);

		// EXPERIMENT 1: TIME GRADIENT SPENDS TO REACH ALL THE NODES

		int counter = 0;
		for (DTNHost host : hosts) {
			if (host.isFlag()) {
				counter++;
			}
		}

		// System.out.println(counter + " have received the gradient.");

		if (!SapereProperties.GRADIENTS_EVALUATION) {
			if (counter == hosts.size() && !convergence) {
				System.out.println(SapereProperties.RUN_NUMBER + ";" + SapereProperties.NODES_NUMBER + ";"
						+ SapereProperties.CYCLE_COUNTER + ";" + SapereProperties.MESSAGE_COUNTER + ";"
						+ SapereProperties.SEED);

				convergence = true;
			}
		} else {
			if (SapereProperties.CYCLE_COUNTER % 10 == 0) {

				createRealGradient();
				double eval = evaluateGradient();

				evaluationCounter++;
				evaluationAvg += eval;

			}
		}

		/* inform all update listeners */
		for (UpdateListener ul : this.updateListeners) {
			ul.updated(this.hosts);
		}
	}

	/**
	 * Updates all hosts (calls update for every one of them). If update order
	 * randomizing is on (updateOrder array is defined), the calls are made in
	 * random order.
	 */
	private void updateHosts() {
		if (this.updateOrder == null) { // randomizing is off
			for (int i = 0, n = hosts.size(); i < n; i++) {
				if (this.isCancelled) {
					break;
				}
				hosts.get(i).update(simulateConnections);
			}
		} else { // update order randomizing is on
			assert this.updateOrder.size() == this.hosts.size() : "Nrof hosts has changed unexpectedly";
			Random rng = new Random(SimClock.getIntTime());
			Collections.shuffle(this.updateOrder, rng);
			for (int i = 0, n = hosts.size(); i < n; i++) {
				if (this.isCancelled) {
					break;
				}
				this.updateOrder.get(i).update(simulateConnections);
			}
		}
	}

	/**
	 * Updates all hosts (calls update for every one of them). If update order
	 * randomizing is on (updateOrder array is defined), the calls are made in
	 * random order.
	 */
	private void updateSapereHosts() {
		if (this.updateOrder == null) { // randomizing is off
			for (int i = 0, n = hosts.size(); i < n; i++) {
				if (this.isCancelled) {
					break;
				}
				for (Application application : hosts.get(i).getRouter().getApplications(AbstractSapereHost.APP_ID)) {
					((AbstractSapereHost) application).sapereUpdate(hosts.get(i));
				}

			}
		} else { // update order randomizing is on
			assert this.updateOrder.size() == this.hosts.size() : "Nrof hosts has changed unexpectedly";
			Random rng = new Random(SimClock.getIntTime());
			Collections.shuffle(this.updateOrder, rng);
			for (int i = 0, n = hosts.size(); i < n; i++) {
				if (this.isCancelled) {
					break;
				}
				for (Application application : hosts.get(i).getRouter().getApplications(AbstractSapereHost.APP_ID)) {
					((AbstractSapereHost) application).sapereUpdate(hosts.get(i));
				}
			}
		}
	}

	/**
	 * Moves all hosts in the world for a given amount of time
	 * 
	 * @param timeIncrement
	 *            The time how long all nodes should move
	 */
	private void moveHosts(double timeIncrement) {
		for (int i = 0, n = hosts.size(); i < n; i++) {
			DTNHost host = hosts.get(i);
			host.move(timeIncrement);
		}
	}

	/**
	 * Asynchronously cancels the currently running simulation
	 */
	public void cancelSim() {
		this.isCancelled = true;
	}

	/**
	 * Returns the hosts in a list
	 * 
	 * @return the hosts in a list
	 */
	public List<DTNHost> getHosts() {
		return this.hosts;
	}

	/**
	 * Returns the x-size (width) of the world
	 * 
	 * @return the x-size (width) of the world
	 */
	public int getSizeX() {
		return this.sizeX;
	}

	/**
	 * Returns the y-size (height) of the world
	 * 
	 * @return the y-size (height) of the world
	 */
	public int getSizeY() {
		return this.sizeY;
	}

	/**
	 * Returns a node from the world by its address
	 * 
	 * @param address
	 *            The address of the node
	 * @return The requested node or null if it wasn't found
	 */
	public DTNHost getNodeByAddress(int address) {
		if (address < 0 || address >= hosts.size()) {
			throw new SimError(
					"No host for address " + address + ". Address " + "range of 0-" + (hosts.size() - 1) + " is valid");
		}

		DTNHost node = this.hosts.get(address);
		assert node.getAddress() == address : "Node indexing failed. " + "Node " + node + " in index " + address;

		return node;
	}

	/**
	 * Schedules an update request to all nodes to happen at the specified
	 * simulation time.
	 * 
	 * @param simTime
	 *            The time of the update
	 */
	public void scheduleUpdate(double simTime) {
		scheduledUpdates.addUpdate(simTime);
	}

	/**
	 * @author Danilo Pianini This methods provides a facility to check whether
	 *         the network is segmented.
	 * @return true if the network is segmented.
	 */
	private boolean networkIsSegmented() {
		/*
		 * Sets do not allow for duplicates. Key feature here.
		 */
		final Set<DTNHost> explored = new HashSet<DTNHost>();
		if (hosts.size() > 0) {
			final HashSet<DTNHost> n = new HashSet<DTNHost>();
			n.add(hosts.get(0));
			while (!n.isEmpty()) {
				final DTNHost h = n.iterator().next();
				explored.add(h);
				n.remove(h);
				for (final Connection c : h.getConnections()) {
					final DTNHost nh = c.getOtherNode(h);
					/*
					 * DEBUG line. TODO: remove when tested.
					 */
					if (nh.equals(h)) {
						System.out.println("It looks like you found a bug!");
					}
					if (!explored.contains(nh)) {
						n.add(nh);
					}
				}
			}
		}
		return hosts.size() != explored.size();
	}

	public boolean isConvergence() {
		return convergence;
	}

	/**
	 * This method evaluate a gradient 0 means that all nodes have a wrong
	 * gradient value. 1 means that all nodes have a correct gradient value.
	 * 
	 * @return
	 */
	private double evaluateGradient() {
		// // TODO Auto-generated method stub
		// double fitness = 0;
		//
		// for (DTNHost host : hosts) {
		// if (nodeList.containsKey(host.getName())) {
		// int realGradientValue = nodeList.get(host.getName());
		// if (host.getName().equals("Node0")) {
		// fitness++;
		// } else if (realGradientValue == host.getGradientValue()) {
		// fitness++;
		// host.setGoodGradient(true);
		// } else {
		// host.setGoodGradient(false);
		//
		// }
		// }
		// }
		//
		// fitness = fitness / nodeList.size();
		// return fitness;
		return 0;
	}

	private boolean createRealGradient() {
		// /*
		// * Sets do not allow for duplicates. Key feature here.
		// */
		//
		// nodeList = new HashMap<String, Integer>();
		// int gradientValue = 0;
		// int nextLevelSize = 0;
		// final ArrayList<DTNHost> explored = new ArrayList<DTNHost>();
		// if (hosts.size() > 0) {
		// final ArrayList<DTNHost> n = new ArrayList<DTNHost>();
		// int levelSize = 1;
		// n.add(hosts.get(0));
		// while (!n.isEmpty()) {
		// final DTNHost h = n.iterator().next();
		// explored.add(h);
		// n.remove(h);
		// nodeList.put(h.getName(), new Integer(gradientValue));
		// levelSize--; // we analise one node...
		// for (final Connection c : h.getConnections()) {
		// final DTNHost nh = c.getOtherNode(h);
		// /*
		// * DEBUG line. TODO: remove when tested.
		// */
		// if (nh.equals(h)) {
		// System.out.println("It looks like you found a bug!");
		// }
		// if (!explored.contains(nh) && !n.contains(nh)) {
		// n.add(nh);
		// nextLevelSize++;
		// }
		//
		// }
		// if (levelSize == 0) {
		// levelSize = nextLevelSize;
		// nextLevelSize = 0;
		// gradientValue++;
		// }
		//
		// }
		// }
		// return hosts.size() != explored.size();
		return true;
	}

}
