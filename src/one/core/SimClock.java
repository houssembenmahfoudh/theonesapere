/*
 * Copyright 2010 Aalto University, ComNet Released under GPLv3. See LICENSE.txt for details.
 */
package one.core;

import java.math.BigDecimal;
import java.math.RoundingMode;

/**
 * Wall clock for checking the simulation time.
 */
public class SimClock {
   private static BigDecimal decimalClock = new BigDecimal("0.0");

   private static SimClock clock = null;

   private SimClock() {
   }

   static {
      DTNSim.registerForReset(SimClock.class.getCanonicalName());
      reset();
   }

   /**
    * Get the instance of the class that can also change the time.
    * @return The instance of this clock
    */
   public static SimClock getInstance() {
      if (clock == null) {
         clock = new SimClock();
      }
      return clock;
   }

   /**
    * Returns the current time (seconds since start)
    * @return Time as a double
    */
   public static double getTime() {
       return decimalClock.doubleValue();
   }

   /**
    * Returns the current time rounded to the nearest integer
    * @return Time as integer
    */
   public static int getIntTime() {
      return decimalClock.intValue();
   }

   /**
    * Advances the time by n seconds
    * @param time Nrof seconds to increase the time
    */
   public void advance(double increment) {
      decimalClock = decimalClock.add(new BigDecimal(increment)).setScale(1, RoundingMode.HALF_UP);
   }

   /**
    * Sets the time of the clock.
    * @param time the time to set
    */
   public void setTime(double time) {
      decimalClock = new BigDecimal(time).setScale(1, RoundingMode.HALF_UP);
   }

   /**
    * Returns the current simulation time in a string
    * @return the current simulation time in a string
    */
   public String toString() {
      return "SimTime: " + decimalClock.toString();
   }

   /**
    * Resets the static fields of the class
    */
   public static void reset() {
      decimalClock = new BigDecimal("0.0");
   }
}
