package one.smartEnergy;

/*
 * 
 * This class generates the SAPERE agents for the simulation
 * 
 */

import one.core.Application;
import one.core.Settings;
import eu.sapere_project.wp3lib.application.SapereApplication;

public final class GeneratorAgent extends SapereApplication {

	public GeneratorAgent(final Settings some_settings) {
		super(some_settings);
	}

	public void setupA0() {
		Payload payload = new Payload("10", "Avail", "Quant", "Type");
		AgentProperties agentProperties = new AgentProperties("agentPropertyA", payload, getNodeManager());
		agentProperties.setInitialLSA();
		// Agent Gradient
		AgentGradient agent = new AgentGradient("A0", getNodeManager(), "here A0spreading!", 10);
		agent.setInitialLSA();

	}

	public void setupAllA(String nodeName) {
		AgentBonding agent = new AgentBonding(nodeName, getNodeManager());
		agent.setInitialLSA();

	}

	public void setupAllB(String nodeName) {
		Payload payload = new Payload("12", "Avail", "Quant", "Type");
		AgentProperties agentProperties = new AgentProperties("agentPropertyB", payload, getNodeManager());
		agentProperties.setInitialLSA();
		AgentBonding agent = new AgentBonding(nodeName, getNodeManager());
		agent.setInitialLSA();
	}

	public void setupAllC(String nodeName) {
		Payload payload = new Payload("8", "Avail", "Quant", "Type");
		AgentProperties agentProperties = new AgentProperties("agentPropertyC", payload, getNodeManager());
		agentProperties.setInitialLSA();
		AgentBonding agent = new AgentBonding(nodeName, getNodeManager());
		agent.setInitialLSA();

	}

	public void setupAllD(String nodeName) {
		Payload payload = new Payload("14", "Avail", "Quant", "Type");
		AgentProperties agentProperties = new AgentProperties("agentPropertyD", payload, getNodeManager());
		agentProperties.setInitialLSA();
		AgentBonding agent = new AgentBonding(nodeName, getNodeManager());
		agent.setInitialLSA();
	}

	public void setupAllE(String nodeName) {
		Payload payload = new Payload("16", "Avail", "Quant", "Type");
		AgentProperties agentProperties = new AgentProperties("agentPropertyE", payload, getNodeManager());
		agentProperties.setInitialLSA();
		AgentBonding agent = new AgentBonding(nodeName, getNodeManager());
		agent.setInitialLSA();
	}

	public void setupAllF(String nodeName) {
		Payload payload = new Payload("6", "Avail", "Quant", "Type");
		AgentProperties agentProperties = new AgentProperties("agentPropertyF", payload, getNodeManager());
		agentProperties.setInitialLSA();
		AgentBonding agent = new AgentBonding(nodeName, getNodeManager());
		agent.setInitialLSA();
	}

	public void setupAll() {

	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Application replicate() {
		return new GeneratorAgent(getSettings());
	}
}
