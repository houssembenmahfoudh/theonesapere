package one.smartEnergy;

/*
 * This is an agent that creates a custom property in its
 * LSA to trigger bond notification with LSAs created by agents 
 * of class AgentGradient. It can be used to read values coming from 
 * other nodes of the network.
 */
import one.core.DTNHost;
import sapere.agent.SapereAgent;
import sapere.lsa.Property;
import sapere.lsa.interfaces.ILsa;
import sapere.lsa.values.PropertyName;
import sapere.node.NodeManager;
import sapere.node.notifier.event.BondAddedEvent;
import sapere.node.notifier.event.BondRemovedEvent;
import sapere.node.notifier.event.BondedLsaUpdateEvent;
import sapere.node.notifier.event.DecayedEvent;
import sapere.node.notifier.event.PropagationEvent;
import sapere.node.notifier.event.ReadEvent;

public class AgentBonding extends SapereAgent {

	private final String agentName;
	private DTNHost myHost;
	private String price;
	private String quantity;
	private String availability;
	private String previous;
	private String sourceGradient;
	private NodeManager nodeManager;
	private AgentInjectorChemotaxis chemotaxisInjector;

	/**
	 * Creates a new agent.
	 * 
	 * @param name
	 *            the name of the node
	 * @param an_opMng
	 *            the operation manager for the node.
	 */
	public AgentBonding(String name, NodeManager a_nodeManager) {
		super(name, a_nodeManager.getOperationManager(), a_nodeManager.getNotifier());
		agentName = name;
		myHost = a_nodeManager.getHost();
		nodeManager = a_nodeManager;
	}

	/**
	 * {@inheritDoc}
	 */
	public void onBondAddedNotification(BondAddedEvent event) {
		// get the reference to the bound LSA
		ILsa boundLsa = event.getBondedLsa();

		System.out.println("**************** " + agentName + " **************** ");
		// System.out.println("Value " +
		// event.getBondedLsa().getProperty(PropertyName.DIFFUSION_OP.toString()));
		if (event.getBondedLsa().getProperty(PropertyName.SOURCE.toString()) != null) {
			sourceGradient = event.getBondedLsa().getProperty(PropertyName.SOURCE.toString()).getValue().firstElement();
		}
		System.out.println("SourceGradient "+ sourceGradient);
		System.out.println("sourceChemo " + event.getBondedLsa().getProperty("sourceChemo"));
		// System.out
		// .println("AGGREGATION_OP " +
		// event.getBondedLsa().getProperty(PropertyName.AGGREGATION_OP.toString()));
		System.out.println("previous " + previous);
		System.out.println("chemoDestination " + event.getBondedLsa().getProperty("chemoDestination"));

		if (event.getBondedLsa().getProperty(PropertyName.DIFFUSION_OP.toString()) != null) {
			if (event.getBondedLsa().getProperty(PropertyName.DIFFUSION_OP.toString()).contains("GRADIENT"))
				this.previous = event.getBondedLsa().getProperty(PropertyName.GRADIENT_PREVIOUS.toString()).getValue()
						.firstElement();
		}

		if (event.getBondedLsa().getProperty("chemoDestination") != null)
			if (agentName.equals(sourceGradient) && event.getBondedLsa().getProperty("chemoDestination").getValue().firstElement().equals(agentName))
				System.out.println("------------------------------------> Received "
						+ event.getBondedLsa().getProperty("sourceChemo"));

		if (boundLsa.getProperty("Availability") != null) {
			Property prop = boundLsa.getProperty("Availability");
			availability = prop.getValue().firstElement();
			// System.out.println(this.agentName + " Availability read: " +
			// availability);
		}
		if (boundLsa.getProperty("Price") != null) {
			Property prop = boundLsa.getProperty("Price");
			price = prop.getValue().firstElement();
			// System.out.println(this.agentName + " Price read: " + price);
		}
		if (boundLsa.getProperty("Quantity") != null) {
			Property prop = boundLsa.getProperty("Quantity");
			quantity = prop.getValue().firstElement();
			// System.out.println(this.agentName + " Quantity read: " +
			// quantity);
		}

		if (boundLsa.getProperty("Type") != null) {
			Property prop = boundLsa.getProperty("Type");
			String value = prop.getValue().firstElement();
			// System.out.println(this.agentName + " Type read: " + value);
		}

		if (boundLsa.getProperty("ReqQuantity") != null) {
			Property prop = boundLsa.getProperty("ReqQuantity");
			String value = prop.getValue().firstElement();
			// System.out.println(this.agentName + " ReqQuantity : " + value + "
			// LocalQuantity: " + quantity);
		}

		if (boundLsa.getProperty("ReqAvailability") != null) {
			Property prop = boundLsa.getProperty("ReqAvailability");
			String value = prop.getValue().firstElement();
			// System.out.println(this.agentName + " ReqAvailability : " + value
			// + " LocalAvailability: " + availability);
		}
		if (boundLsa.getProperty("ReqPrice") != null && price != null) {
			Property prop = boundLsa.getProperty("ReqPrice");
			String value = prop.getValue().firstElement();
			// System.out.println(this.agentName + " ReqPrice : " + value + "
			// LocalPrice: " + price);
			if (Integer.parseInt(value) > Integer.parseInt(price)) {
				chemotaxisInjector = new AgentInjectorChemotaxis(agentName, previous, nodeManager);
				chemotaxisInjector.setInitialLSA();

			}
		}

		// if (event.getBondedLsa().getProperty("chemoDestination") != null &&
		// chemotaxisInjector == null
		// && event.getBondedLsa().getProperty("sourceChemo") != null) {
		if (!event.getBondedLsa().getProperty("sourceChemo").getValue().firstElement().equals(agentName)
				&& chemotaxisInjector == null) {
			chemotaxisInjector = new AgentInjectorChemotaxis(
					event.getBondedLsa().getProperty("sourceChemo").getValue().firstElement(), previous, nodeManager);
			chemotaxisInjector.setInitialLSA();
		}

		// read the property of the bound LSA
		Property prop = boundLsa.getProperty("gradient");
		// this is the property value that has been spread
		String value = prop.getValue().firstElement();
		// we get also the hop counter value of the gradient
		Property graProp = boundLsa.getProperty(PropertyName.GRADIENT_HOP.toString());
		String grValue = graProp.getValue().firstElement();
		int gr = Integer.parseInt(grValue);

		// we update the hop counter for this specific gradient id
		// NOTE: we do it to visualize it in the simulator.
		graProp = boundLsa.getProperty(PropertyName.GRADIENT.toString());
		String gId = graProp.getValue().firstElement();

		this.myHost.insertGradientValue(gId, gr);
		// System.out.println(this.agentName + " gValue: " + myHost.gValue);
		// System.out.println(this.agentName + " property read: " + value + "
		// g=" + grValue);
	}

	/**
	 * {@inheritDoc}
	 */
	public void onBondRemovedNotification(BondRemovedEvent event) {
		String gValue = event.getCopyRemovedLsa().getProperty(PropertyName.GRADIENT_HOP.toString()).getValue()
				.firstElement();

		System.out.println(this.agentName + " removed old gradient " + gValue);
	}

	/**
	 * {@inheritDoc}
	 */
	public void onBondedLsaUpdateEventNotification(BondedLsaUpdateEvent event) {
	}

	/**
	 * {@inheritDoc}
	 */
	public void onPropagationEvent(PropagationEvent event) {
	}

	/**
	 * {@inheritDoc}
	 */
	public void setInitialLSA() {
		System.out.println("Created agent " + agentName);

		// we want to read a value placed in a property named "gradient"
		addSubDescription("sub", new Property("gradient", "*"));
		addSubDescription("sub0", new Property("source", "*"));
		addSubDescription("sub2", new Property("Price", "*"));
		addSubDescription("sub3", new Property("Availability", "*"));
		addSubDescription("sub4", new Property("Quantity", "*"));
		addSubDescription("sub5", new Property("Type", "*"));
		addSubDescription("sub6", new Property("ReqPrice", "*"));
		addSubDescription("sub7", new Property("ReqQuantity", "*"));
		addSubDescription("sub8", new Property("ReqAvailability", "*"));

		this.addSubDescription("chemo2", new Property("chemoDestination", "*"));
		this.addSubDescription("sourceChemo", new Property("sourceChemo", "*"));

		// NOTE: do not forget to invoke this method to
		// update the LSA of the agent
		submitOperation();
	}

	public void onReadNotification(ReadEvent readEvent) {
	}

	public void onDecayedNotification(DecayedEvent event) {
	}
}